
 var current_path = window.location.href;
 current_path = current_path.split('/');
 current_path = current_path[current_path.length - 1];
 //donation manage
 if (current_path == 'manageDonation.php') {
 	manage_Donation();
 	function manage_Donation() {
 		$.ajax({
 			url: 'Functions/Manage.php',
 			type:'POST',
			data: {donation: 'managedonation'},
 			success:function (result) {

 			var donation_Data = JSON.parse(result);

 			var html = '';
 
 			$(donation_Data).each(function(i,e){
				
				html +=	'<tr role="row" >'+
						'<td class="sorting_1">'+e.name+'</td>'+
						'<td>'+e.title+'</td>'+
						'<td>'+e.amount+'</td>'+
						'<td>'+e.paymentmode+'</td>'+
						'<td>'+e.number+'</td>'+
						'<td>'+e.donatedate+'</td>'+
						'<td>'+
						'<a href="javascript:;" class="label label-warning" onclick="donation_Detail_Modal('+e.id+');">Edit</a>'+
						'<a href="javascript:;" class="label label-info" id="item_delete" data="'+e.id+'">Delete</a>'+
						'</td>'+
						'<td> <a class="example-image-link" href="assets/img/donations/scannedimg/'+e.scannedimg+'" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img src="assets/img/donations/scannedimg/'+e.scannedimg+'" height="50px" width="50px"></a></td>'
						'</tr>';

			});
      
            $('#donation_Record').html(html);
                        // $('#donation_Record').html('');
                       
            }
 		})
 		
 		
 	}
 	manage_Donation();
 

 }
 //Manage Donor 
  if (current_path == 'manageDonor.php') {
  manage_Donor();
  function manage_Donor() {
    $.ajax({
      url: 'Functions/Manage.php',
      type:'POST',
      data: {donor: 'managedonor'},
      success:function (result) {

      var donor_Data = JSON.parse(result);

      var html = '';

      $(donor_Data).each(function(i,e){
        
        html += '<tr role="row" >'+
            '<td class="sorting_1">'+e.name+'</td>'+
            '<td>'+e.email+'</td>'+
            '<td>'+e.phone+'</td>'+
            '<td>'+e.designation+'</td>'+
            '<td>'+e.about+'</td>'+
            '<td>'+e.address+'</td>'+
            '<td>'+e.companydetail+'</td>'+
            '<td>'+e.profession+'</td>'+
            '<td>'+e.city+'</td>'+
            '<td>'+e.volunteername+'</td>'+
            
            '<td> <a class="example-image-link" href="assets/img/donors/profile/'+e.profile+'" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img src="assets/img/donors/profile/'+e.profile+'" height="50px" width="50px"></a></td>'
            +'<td>'+
            '<a href="javascript:;" class="label label-warning" onclick="donor_Detail_Modal('+e.id+');">Edit</a>'+
            '<a href="javascript:;" class="label label-info" id="item_delete_donor" data="'+e.id+'">Delete</a>'+
            '</td>'

            '</tr>';

      });
                        // $('#donation_Record').html('');
                        $('#donor_Record').html(html);
            }
    })
    
    
  }
  manage_Donor();
 

 }  if (current_path == 'managevolunteer.php') {
  manage_volunteer();
  function manage_volunteer() {
    $.ajax({
      url: 'Functions/Manage.php',
      type:'POST',
      data: {volunteer: 'managevolunteer'},
      success:function (result) {

      var volunteer_Data = JSON.parse(result);

      var html = '';

      $(volunteer_Data).each(function(i,e){
        
        html += '<tr role="row" >'+
            '<td class="sorting_1">'+e.name+'</td>'+
            '<td>'+e.joiningdate+'</td>'+
            '<td>'+e.servicearea+'</td>'+
            '<td>'+e.areacode+'</td>'+
            '<td>'+e.email+'</td>'+
            '<td>'+e.biodata+'</td>'+
            '<td>'+e.cv+'</td>'+
            '<td>'+e.phone+'</td>'+
            '<td>'+e.designation+'</td>'+
            '<td>'+e.city+'</td>'+
            '<td>'+e.address+'</td>'+
            '<td>'+e.salary+'</td>'+
            
            '<td> <a class="example-image-link" href="assets/img/donors/profile/'+e.profile+'" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img src="assets/img/donors/profile/'+e.profile+'" height="50px" width="50px"></a></td>'
            +'<td>'+
            '<a href="javascript:;" class="label label-warning" onclick="donor_Detail_Modal('+e.id+');">Edit</a>'+
            '<a href="javascript:;" class="label label-info" id="item_delete_donor" data="'+e.id+'">Delete</a>'+
            '</td>'

            '</tr>';

      });
                        // $('#donation_Record').html('');
                        $('#volunteer_Record').html(html);
            }
    })
    
    
  }
  manage_volunteer();
 

 }
 // Edit modal of Donation Detail
  function donation_Detail_Modal(id){
     var data = {"id" : id};
   
    $.ajax({
      url: 'Modals/Editmodeldonation.php',
      type: 'POST',
      data: data,
      success:function(data){
        jQuery('body').append(data);
        jQuery('#detailModal').modal('toggle');
      },
      error: function() {
        alert("Something went Wrong");
      }
    });

   }
   function donor_Detail_Modal(id){
     var data = {"id" : id};
   
    $.ajax({
      url: 'Modals/Editmodeldonor.php',
      type: 'POST',
      data: data,
      success:function(data){
        jQuery('body').append(data);
        jQuery('#detailModal').modal('toggle');
      },
      error: function() {
        alert("Something went Wrong");
      }
    });

   }
   $(document).ready(function() {
    // manage_Donation();
   	// Delete Donation 
   $('#donation_Record').on('click', '#item_delete', function() {
    var id = $(this).attr('data');
	$('#deletemodal').modal('show');
	$('#btndelete').unbind().click(function() {
		$.ajax({
		url: 'Functions/Delete.php?form=deletedonation',
		method: 'POST',
		data: {id: id},
		async: false,
		success:function(response){
		
			if (response == 'yes') {
				$('#deletemodal').modal('hide');
				$('.alert-success').html(' Delete Succesfully').fadeIn().delay(4000).fadeOut('slow');
				manage_Donation();
			}
			else{
				alert('delete error');
			}
		},
		error:function(){
			alert('Something Went Wrong');
		}
	})
	});
	});
 
  // $("#update_Donation").on('submit', function(e) {
         
  //   e.preventDefault();
  //              $.ajax({
  //                  url: 'Functions/Update.php?form=updatedonation',
  //                   type: "POST",             
  //                   data: new FormData(this), 
  //                   contentType: false,       
  //                   cache: false,             
  //                   processData:false,        
  //                   success:function (result) {
  //                     alert(result);
  //                     if (result == 'yes') {
  //                       $('#detailModal').modal('hide');
  //       $('.alert-success').html('Employee record Update Succesfully').fadeIn().delay(4000).fadeOut('slow');
  //       manage_Donation();
  //                     }
  //                     else{
  //                       $('#alert_message').html('Something Went Wrong');
  //                     }
  //             // $('').html(result);
  //                   }
  //               })
  //  });
   });
   $(document).ready(function() {
  
    // Delete Donation 
   $('#donor_Record').on('click', '#item_delete_donor', function() {
    var id = $(this).attr('data');
  $('#deletemodal').modal('show');
  $('#btndelete').unbind().click(function() {
    $.ajax({
    url: 'Functions/Delete.php?form=deletedonor',
    method: 'POST',
    data: {id: id},
    async: false,
    success:function(response){
   
    
      if (response == 'yes') {
        $('#deletemodal').modal('hide');
        $('.alert-success').html(' Delete Succesfully').fadeIn().delay(4000).fadeOut('slow');
        manage_Donor();
      }
      else{
        alert('delete error');
      }
    },
    error:function(){
      alert('Something Went Wrong');
    }
  })
  });
  });

 $("#partner_Assessment").on('submit', function(e) {
         
    e.preventDefault();
               $.ajax({
                   url: 'Functions/Insert.php?form=partnerAssessment',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
                      // alert(result);
                      $('.alert-success').html(result);
                      $('html, body').animate({
                            scrollTop: $('#partner_Assessment').offset().top,
                        },2000); 
                    }
                })
   });

  $("#partner_Donation").on('submit', function(event) {
    event.preventDefault();
    $.ajax({
                   url: 'Functions/Insert.php?form=partnerDonation',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
                    $('.alert-success').html(result);
                    $('#form_uploadPreview').trigger('reset');
                      $('html, body').animate({
                            scrollTop: $('#partner_Donation').offset().top,
                        },2000); 
                    }
                })
  });

  
   });
   