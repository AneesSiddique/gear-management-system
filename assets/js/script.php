<script type="text/javascript">
 var current_path = window.location.href;
 current_path = current_path.split('/');
 current_path = current_path[current_path.length - 1];
 //donation manage
 if (current_path == 'manageDonation.php') {
 	manage_Donation();
 	function manage_Donation() {
 		$.ajax({
 			url: 'Functions/Manage.php',
 			type:'POST',
			data: {donation: 'managedonation'},
 			success:function (result) {

 			var donation_Data = JSON.parse(result);

 			var html = '';

 			$(donation_Data).each(function(i,e){
				
				html +=	'<tr role="row" >'+
						'<td class="sorting_1">'+e.name+'</td>'+
						'<td>'+e.title+'</td>'+
						'<td>'+e.amount+'</td>'+
						'<td>'+e.paymentmodeid+'</td>'+
						'<td>'+e.number+'</td>'+
						'<td>'+e.donatedate+'</td>'+
						'<td>'+
						'<a href="javascript:;" class="label label-warning" onclick="donation_Detail_Modal('+e.id+');">Edit</a>'+
						'<a href="javascript:;" class="label label-info" id="item_delete" data="'+e.id+'">Delete</a>'+
						'</td>'+
						'<td> <a class="example-image-link" href="assets/images/slip.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img src="assets/images/slip.jpg" height="50px" width="50px"></a></td>'
						'</tr>';

			});
                        // $('#donation_Record').html('');
                        $('#donor_Record').html(html);
            }
 		})
 		
 		
 	}
 	manage_Donation();
 

 }
 // Edit modal of Donation Detail
  function donation_Detail_Modal(id){
     var data = {"id" : id};
   
    $.ajax({
      url: 'Modals/ViewDonationDetails.php',
      type: 'POST',
      data: data,
      success:function(data){
        jQuery('body').append(data);
        jQuery('#detailModal').modal('toggle');
      },
      error: function() {
        alert("Something went Wrong");
      }
    });

   }
   $(document).ready(function() {
   	// Delete Donation 
   $('#donation_Record').on('click', '#item_delete', function() {
    var id = $(this).attr('data');
	$('#deletemodal').modal('show');
	$('#btndelete').unbind().click(function() {
		$.ajax({
		url: 'Functions/Delete.php',
		method: 'POST',
		data: {id: id,action:'deletedonation'},
		async: false,
		success:function(response){
		
			if (response == 'yes') {
				$('#deletemodal').modal('hide');
				$('.alert-success').html('Employee record Delete Succesfully').fadeIn().delay(4000).fadeOut('slow');
				manage_Donation();
			}
			else{
				alert('delete error');
			}
		},
		error:function(){
			alert('Something Went Wrong');
		}
	})
	});
	});
 
  // $("#update_Donation").on('submit', function(e) {
         
  //   e.preventDefault();
  //              $.ajax({
  //                  url: 'Functions/Update.php?form=updatedonation',
  //                   type: "POST",             
  //                   data: new FormData(this), 
  //                   contentType: false,       
  //                   cache: false,             
  //                   processData:false,        
  //                   success:function (result) {
  //                     alert(result);
  //                     if (result == 'yes') {
  //                       $('#detailModal').modal('hide');
  //       $('.alert-success').html('Employee record Update Succesfully').fadeIn().delay(4000).fadeOut('slow');
  //       manage_Donation();
  //                     }
  //                     else{
  //                       $('#alert_message').html('Something Went Wrong');
  //                     }
  //             // $('').html(result);
  //                   }
  //               })
  //  });
   });
   </script>