<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?><body>

	<!-- Main navbar -->
	<?php include('includes/nav.php') ?>
		<?php include('includes/connection.php') ?>


	<!-- /main navbar -->
<?php include('includes/sidebar_nav.php') ?>


	<!-- Page container -->
	


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-md-8">

							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add New Accounts</h5>
								</div>
								<div id="alert_message"></div>

								<div class="panel-body">
									<form class="form-horizontal" enctype='multipart/form-data' method="post" id='addnewaccount' action="#">
										<div class="form-group">
											<label class="control-label col-lg-2">Volunteer</label>
											<div class="col-lg-10">
												 <select name="volunteer" id="Volunteer" required=""  class="select-multiple-tokenization">
				                        		 	 <option label="Select Refrence"></option>
				           						<?php
				            $select_volunteers=mysqli_query($con,"Select * from volunteers");
				                while($fetch_volunteers =mysqli_fetch_array($select_volunteers)){
				           		echo" <option value='".$fetch_volunteers['id']."'>".$fetch_volunteers['name']."</option>";
					                                } ?>
					                            </select>
											</div>
										</div>

									<div class="form-group">
				                        	<label class="control-label col-lg-2">Expense Type</label>
				                        	<div class="col-lg-10">
					                           <input type="text" required="" class="form-control" name="expensetype">
				                            </div>
				                        </div>

										<div class="form-group">
											<label class="control-label col-lg-2">Amount</label>
											<div class="col-lg-10">
												<input type="text" name="amount" required="" class="form-control">
											</div>
										</div>
										<div class="form-group">
				                        	<label class="control-label col-lg-2">Payment Mode</label>
				                        	<div class="col-lg-10">
					                            <select name="paymentmode" required="" class="form-control paymentmode">
					                               	<option label="Select Payment Mode"></option>
					                                <option value="cash">Cash</option>
					                                <option value="cheque">Cheque</option>
					                                <option value="easypaisa">Easypaisa</option>
					                                <option value="banktransfer">Bank Transfer</option>
					                                <option value="other">Other</option>
					                            </select>
				                            </div>
				                        </div>
				                        <div class="form-group" style="display:none " id="Cheque">
  <label  class="control-label col-lg-2">Number</label>
  <div class="col-lg-10">
Cheque Number
<input type="text"  name="number" class="form-control">
</div>
</div>
  
<div class="form-group" style="display:none " id="bank">
  <label  class="control-label col-lg-2">Number</label>
  <div class="col-lg-10">
Bank Transfer Number
<input type="text"  name="numbertwo" class="form-control">
</div>
</div>

 <div class="form-group" style="display:none " id="other">
  <label  class="control-label col-lg-2">Number</label>
  <div class="col-lg-10">
Other Number
<input type="text"  name="numberthree" class="form-control">
Other Payment Name
<input type="text"   name="OtherPaymentName" class="form-control">
</div>
</div>

										
				                         <div class="form-group" id="hideshow" style="display: none">
				                          	<label class="control-label col-lg-2">&nbsp;</label>
				                          	 <img class="img img-responsive"   id="uploadPreview"  height="100" width="150" class="img-thumb-modal">
				                          	</div>
				                          <div class="form-group">
				                        	<label class="control-label col-lg-2">Upload Proof image</label>
				                        	<div class="col-lg-10">
				                        										
											
												<input type="file"  accept="image/x-png, image/gif, image/jpeg" id="uploadImage" onchange="PreviewImage('uploadImage','uploadPreview')" name="proof" required="" class="btn btn-info form-control">
											</div>
										</div>

										

										<div class="text-right">
											<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
						<div class="col-lg-2"></div>
					</div>
				
					



					<!-- Footer -->
					<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
	   function PreviewImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);
            $('#hideshow').css('display', 'block');

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
$(document).ready(function(){
    $("select.paymentmode").change(function(){
        var selectedpaymentmode = $(".paymentmode option:selected").val();
        if(selectedpaymentmode=='cheque' || selectedpaymentmode=='banktransfer' || selectedpaymentmode=='other'){
         if(selectedpaymentmode=='cheque'){
         $("#Cheque").css('display', 'block');
          $("#bank").css('display', 'none');
           $("#other").css('display', 'none');
         
         } 
         if(selectedpaymentmode=='banktransfer'){
         $("#bank").css('display', 'block');
          $("#Cheque").css('display', 'none');      
           $("#other").css('display', 'none');
              
         }
         if(selectedpaymentmode=='other'){
        $("#bank").css('display', 'none');
          $("#Cheque").css('display', 'none');      
           $("#other").css('display', 'block');
            
         }}
         else{

         	 $("#bank").css('display', 'none');
          $("#Cheque").css('display', 'none');      
           $("#other").css('display', 'none');
               $("#num").css('display', 'none');
         }

       

    });
});
 $("#addnewaccount").on('submit', function(e) {
              e.preventDefault();
               $.ajax({
                   url: 'Functions/Insert.php?form=addnewaccount',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
              $('#alert_message').html(result);
                    }
                })
               
                
            }); 
</script>
</body>
</html>
<?php } ?>
