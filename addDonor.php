
<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>
<body>

	<?php include('includes/nav.php') ?>
	<?php include('includes/connection.php') ?>

	<!-- /main navbar -->


	<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-md-8">

							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add New Donation</h5>
								</div>
								 <div id="alert_message"></div>
								<div class="panel-body">
									<form class="form-horizontal"  enctype='multipart/form-data' method="post" id='adddonor' action="#">

									<div class="form-group">
											<label class="control-label col-lg-2">Donor Name</label>
											<div class="col-lg-10">
												<input type="text" name="name" required="" class="form-control">
											</div>
										</div>	


									<div class="form-group">
											<label class="control-label col-lg-2">Email</label>
											<div class="col-lg-10">
												<input type="email" name="email" required="" class="form-control">
											</div>
										</div>

									<div class="form-group">
											<label class="control-label col-lg-2">Phone</label>
											<div class="col-lg-10">
												<input type="text" name="phone" required="" class="form-control">
											</div>
										</div>	
										<div class="form-group">
											<label class="control-label col-lg-2">Designation</label>
											<div class="col-lg-10">
												<input type="text" name="designation" required="" class="form-control">
											</div>
										</div>	
										<div class="form-group">
											<label class="control-label col-lg-2">Refrence</label>
											<div class="col-lg-10">
												 <select name="refrence" id="refrence" required=""  class="select-multiple-tokenization">
				                        		 	 <option label="Select Refrence"></option>
				           						<?php
				            $select_volunteers=mysqli_query($con,"Select * from volunteers");
				                while($fetch_volunteers =mysqli_fetch_array($select_volunteers)){
				           		echo" <option value='".$fetch_volunteers['id']."'>".$fetch_volunteers['name']."</option>";
					                                } ?>
					                            </select>
											</div>
										</div>
										 <div class="form-group" id="hideshow" style="display: none">
				                          	<label class="control-label col-lg-2">&nbsp;</label>
				                          	 <img class="img img-responsive"   id="uploadPreview"  height="100" width="150" class="img-thumb-modal">
				                          	</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Profile Picture</label>
											<div class="col-lg-10">
												<input type="file" id="uploadImage" onchange="PreviewImage('uploadImage','uploadPreview')" name="profile" required="" class="form-control btn btn-success">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">how he got to know about</label>
											<div class="col-lg-10">
												<input type="text" name="howhegottoknowabout" required="" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Address</label>
											<div class="col-lg-10">
												<textarea class="form-control" name="address" required=""></textarea>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-lg-2">Company Details</label>
											<div class="col-lg-10">
												<textarea class="form-control" required="" name="companydetails"></textarea>
											</div>
										</div>
										
									<div class="form-group">
				                        	<label class="control-label col-lg-2">Profession</label>
				                        	<div class="col-lg-10">
					                             <select name="profession" id="profession" required=""  class="select-multiple-tokenization">
				                        		 	 <option label="Select Profession"></option>
				           						<?php
				            $select_professions=mysqli_query($con,"Select * from professions");
				                while($fetch_professions =mysqli_fetch_array($select_professions)){
				           		echo" <option value='".$fetch_professions['id']."'>".$fetch_professions['profession']."</option>";
					                                } ?>
					                            </select>
				                            </div>
				                        </div>
				                        <div class="form-group">
				                        	<label class="control-label col-lg-2">City</label>
				                        	<div class="col-lg-10">
					                             <select name="city" id="city" required=""  class="select-multiple-tokenization">
				                        		 	 <option label="Select City"></option>
				           						<?php
				            $select_cities=mysqli_query($con,"Select * from cities");
				                while($fetch_cities =mysqli_fetch_array($select_cities)){
				           		echo" <option value='".$fetch_cities['id']."'>".$fetch_cities['city']."</option>";
					                                } ?>
					                            </select>
				                            </div>
				                        </div>

										
									
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
						<div class="col-lg-2"></div>
					</div>
				
					



					<!-- Footer -->
						<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<script type="text/javascript">
    function PreviewImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);
            $('#hideshow').css('display', 'block');

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }

	  $("#adddonor").on('submit', function(e) {
              e.preventDefault();
               $.ajax({
                   url: 'Functions/Insert.php?form=adddonor',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
              $('#alert_message').html(result);
                    }
                })
               
                
            }); 
	
</script>
</body>
</html>
<?php } ?>