<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>
<script type="text/javascript">
	function PreviewImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
</script>
<body>
	<!-- Main navbar -->
<?php include('includes/nav.php') ?>
	<!-- /main navbar -->



<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->
			

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-md-12">

							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Parthner Assements</h5>
								</div>
								
								<div class="panel-body">
									<form class="form-horizontal" method="POST" id="partner_Assessment" enctype="multipart/form-data">
									<div class="alert-success" ></div>
									<div class="form-group">
											<label class="control-label col-lg-2">Application Holder</label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="application_Holder">
											</div>
										</div>	
										<div class="form-group">
											<label class="control-label col-lg-2">CNIC NO#</label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="cnic_No">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Gender</label>
											<div class="col-lg-10">
												<input type="radio" name="gender" value="male"> Male&nbsp;&nbsp;
												<input type="radio" name="gender" value="female"> Female
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Contact Number</label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="contact_No"> 
												
											</div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-2">Current Address</label>
										<div class="col-lg-10">
												<input type="text" class="form-control" name="current_Address"> 
										<label class="radio-inline">
											<input type="radio" value="rent" name="Address" checked="checked">
											Home On Rent
										</label>

										<label class="radio-inline">
											<input type="radio" value="owner" name="Address">
											Own Home
										</label>
											</div>
										
									</div>
									
										<div class="form-group">
											<label class="control-label col-lg-2">Qualification</label>
											<div class="col-lg-10">
											<label class="radio-inline">
											<input type="radio" value="uneducatated" name="qualification" checked="checked">
											Uneducatated
											</label>
											<label class="radio-inline">
											<input type="radio" value="primary" name="qualification" checked="checked">
											Primary
											</label>
											<label class="radio-inline">
											<input type="radio" value="matric" name="qualification" checked="checked">
											Matric
											</label>
											<label class="radio-inline">
											<input type="radio" value="intermediate" name="qualification" checked="checked">
											Intermediate
											</label>
											<label class="radio-inline">
											<input type="radio" value="graduated" name="qualification" checked="checked">
											Graduated
											</label>
												
											</div>
										</div>
										<label style="font-size: 20px" class="control-label col-lg-12">CNIC Front And Back Image</label>
										<div class="row">
											<div class="col-md-6">
												
													<div  style="border: 12px; height: 200px; border-color: black; border:solid;">
											 <div align="center">
												FORM BACK
											</div>
											 <img  id="uploadPreview"  height="200" width="100%">
											
											</div>
											<div class="form-group">
											<input id="uploadImage"  onchange="PreviewImage('uploadImage','uploadPreview')" type="file" class="form-control" name="cnic_back">
												</div>
												</div>
												<div class="col-md-6">
													
														<div  style="border: 12px; height: 200px; border-color: black; border:solid;">
													<div align="center">
												FORM FRONT
											</div>
													<img  id="front_uploadPreview"  height="200" width="100%">
													
												</div>
												<div class="form-group">
												<input id="front_uploadImage"  onchange="PreviewImage('front_uploadImage','front_uploadPreview')" type="file" class="form-control " name="cnic_Front">
													</div>
												</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Maritial Status</label>
											<div class="col-lg-10">
											<label class="radio-inline">
											<input type="radio" value="uneducatated" name="marital_status" checked="checked">
											UnMarried
											</label>
											<label class="radio-inline">
											<input type="radio" value="primary" name="marital_status" checked="checked">
											Married
											</label>
											<label class="radio-inline">
											<input type="radio" value="matric" name="marital_status" checked="checked">
											Widow
											</label>
											
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-lg-2">
												Current income to increase the bussiness information
											</label>
											<div class="col-lg-10">
												<div class="row">
													<div class="col-md-4">
														<label class="control-label col-lg-6">Total People</label>
														<div class="col-md-6">
															<input type="text" name="total_people" class="form-control">
														</div>
													</div>
													<div class="col-md-4">
														<label class="control-label col-lg-6">Total Income</label>
														<div class="col-md-6">
															<input type="text" name="total_income" class="form-control">
														</div>
													</div>
													<div class="col-md-4">
														<label class="control-label col-lg-6">Total Kids</label>
														<div class="col-md-6">
															<input type="text" name="total_kids" class="form-control">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div>
											<div class="row">
											<div class="col-md-6">
											<label style="font-size: 20px" class="control-label col-lg-12">Family Monthly Expensis Detail</label>
										</div>
										<!-- <div class="col-md-6">
											<i  id="add_Family_detail" class="icon-plus-circle2 icon-3x opacity-75"></i>
										</div> -->
										</div>
												<table class="table table-responsive table-striped" id="family_detail">
													<tr>
														<td>Name</td>
														<td>Relation</td>
														<td>Nature Of Work</td>
														<td>Monthly Income</td>
														<td>Monthy Family Expensis</td>
														<td>How Work Start ?</td>
														<td>Expected Monthly Amount</td>
													</tr>
													<tr>
									<td><input type="text" class="form-control" name="user[person_name][]"></td>
									<td><input type="text" class="form-control" name="user[relation_with][]"></td>
									<td><input type="text" class="form-control" name="user[work_nature][]"></td>
									<td><input type="text" class="form-control" name="user[monthly_income][]"></td>
									<td><input type="text" class="form-control" name="user[monthly_family_expensis][]"></td>
									<td><input type="text" class="form-control" name="user[how_work][]"></td>
									<td><input type="text" class="form-control" name="user[expected_amount][]"></td>
									</tr>
										<tr>
									<td><input type="text" class="form-control" name="user[person_name][]"></td>
									<td><input type="text" class="form-control" name="user[relation_with][]"></td>
									<td><input type="text" class="form-control" name="user[work_nature][]"></td>
									<td><input type="text" class="form-control" name="user[monthly_income][]"></td>
									<td><input type="text" class="form-control" name="user[monthly_family_expensis][]"></td>
									<td><input type="text" class="form-control" name="user[how_work][]"></td>
									<td><input type="text" class="form-control" name="user[expected_amount][]"></td>
									</tr>
										<tr>
									<td><input type="text" class="form-control" name="user[person_name][]"></td>
									<td><input type="text" class="form-control" name="user[relation_with][]"></td>
									<td><input type="text" class="form-control" name="user[work_nature][]"></td>
									<td><input type="text" class="form-control" name="user[monthly_income][]"></td>
									<td><input type="text" class="form-control" name="user[monthly_family_expensis][]"></td>
									<td><input type="text" class="form-control" name="user[how_work][]"></td>
									<td><input type="text" class="form-control" name="user[expected_amount][]"></td>
									</tr>
										<tr>
									<td><input type="text" class="form-control" name="user[person_name][]"></td>
									<td><input type="text" class="form-control" name="user[relation_with][]"></td>
									<td><input type="text" class="form-control" name="user[work_nature][]"></td>
									<td><input type="text" class="form-control" name="user[monthly_income][]"></td>
									<td><input type="text" class="form-control" name="user[monthly_family_expensis][]"></td>
									<td><input type="text" class="form-control" name="user[how_work][]"></td>
									<td><input type="text" class="form-control" name="user[expected_amount][]"></td>
									</tr>


													</table>
													</div>
													<br>
													<div class="form-group">
											<label class="control-label col-lg-2">Why Are Need To Loan ?</label>
											<div class="col-lg-10">
												<textarea class="form-control" name="why_Need_Loan"></textarea>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6">
											<label style="font-size: 20px" class="control-label col-lg-12">Children Details</label>
										</div>
										<div class="col-md-6">
											<i  id="children_more_add" class="icon-plus-circle2 icon-3x opacity-75"></i>
										</div>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<div class="row">
													<div id="children_data">
													<div class="col-md-12">
														<div class="col-md-3">
														<label class="control-label col-lg-12">Name</label>
														<div class="col-md-12">
															<input type="text" name="children[child_Name][]" class="form-control">
														</div>
													</div>
													<div class="col-md-3">
														<label class="control-label col-lg-12">Age</label>
														<div class="col-md-12">
															<input type="text" name="children[child_Age][]" class="form-control">
														</div>
													</div>
													<div class="col-md-3">
														<label class="control-label col-lg-12">Class</label>
														<div class="col-md-12">
															<input type="text" name="children[school_Class_Name][]" class="form-control">
														</div>
													</div>
													</div>
													</div>
													
												</div>
											</div>
										</div>
										<div class="form-group">
									<label class="control-label col-lg-12">Are You Facing Any Problem To Education in kids ?</</label>
									<div class="col-md-12">
										<textarea class="form-control" rows="4" name="problum_Facing_Education"></textarea>
									</div>
										</div>
										
										<label style="font-size: 20px" class="control-label col-lg-12">Loan/Zakat Information</label>
										<div class="form-group">
											<label class="control-label col-lg-2">Required</label>
											<div class="col-lg-10">
												<label class="radio-inline">
											<input type="radio" value="loan" name="required" checked="checked">
											Loan
											</label>
											<label class="radio-inline">
											<input type="radio" value="zakat" name="required" checked="checked">
											Zakat
											</label>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Amount</label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="amount">
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-lg-2">Put Amount In Words</label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="amount_In_Words">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Loan Pay Time</label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="pay_time">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Monthly Stallment</label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="monthly_Stallment">
											</div>
										</div>
										<label style="font-size: 20px" class="control-label col-lg-12">Filled Assessment Form Front And Back Image</label>
										<div class="row">
											<div class="col-md-6">
												
													<div style="border: 12px; height: 200px; border-color: black; border:solid;">
													<td >
											</td>
											<div align="center">
												FORM BACK
											</div>
											 <img  id="form_backuploadPreview"  height="200" width="100%">
											</div>
											<div class="form-group">
											<input id="form_backuploadImage"  onchange="PreviewImage('form_backuploadImage','form_backuploadPreview')" type="file" class="form-control" name="form_back">
												</div>
												</div>
												<div class="col-md-6">
													
														<div  style="border: 12px; height: 200px; border-color: black; border:solid;">
													<td></td>
													<div align="center">
												FORM FRONT
											</div>
											 <img  id="form_uploadPreview"  height="200" width="100%">
												</div>
												<div class="form-group">
												<input id="form_uploadImage"  onchange="PreviewImage('form_uploadImage','form_uploadPreview')" type="file" class="form-control " name="form_Front">
													</div>
												</div>
										</div>	
										
									
										<div class="text-right">
											<button type="submit" id="partner_Assessment_Submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
						<div class="col-lg-2"></div>
					</div>
				
					



					<!-- Footer -->
						<?php include('includes/footer.php') ?>
						<script type="text/javascript">
							$(document).ready(function() {
								var i=1;
	$('#children_more_add').click(function(){
		
		i++;
		$('#children_data').append('<div class="col-md-12" id="row'+i+'">'+
										'<div class="col-md-3">'+
												'<label class="control-label col-lg-12">Name</label>'+
												'<div class="col-md-12">'+
												'<input type="text" name="children[child_Name][]" class="form-control">'+
												'</div>'+
											'</div>'+
											'<div class="col-md-3">'+
												'<label class="control-label col-lg-12">Age</label>'+
												'<div class="col-md-12">'+
											'<input type="text" name="children[child_Age][]" class="form-control">'+
												'</div>'+
											'</div>'+
											'<div class="col-md-3">'+
												'<label class="control-label col-lg-12">Class</label>'+
												'<div class="col-md-12">'+
												'<input type="text" name="children[school_Class_Name][]" class="form-control">'+
												'</div>'+
												'<div class="col-md-3">'+
					'<button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove" >X</button>'+
												'</div>'+
											'</div>'+
											'</div>'
				);
	});
	// var i=1;
	// $('#add_Family_detail').click(function(){
		
	// 	i++;
	// 	$('#family_detail').append(	'<tr id="row'+i+'">'+
	// 								'<td><input type="text" class="form-control" name="person_Name"></td>'+
	// 								'<td><input type="text" class="form-control" name="relation_With"></td>'+
	// 								'<td><input type="text" class="form-control" name="work_Nature"></td>'+
	// 								'<td><input type="text" class="form-control" name="monthly_Income"></td>'+
	// 								'<td><input type="text" class="form-control" name="monthly_Family_Expensis"></td>'+
	// 								'<td><input type="text" class="form-control" name="how_work"></td>'+
	// 								'<td><input type="text" class="form-control" name="expected_Amount"></td>'+
	// 								'</tr>'+
	// 												);
	// });


							$(document).on('click', '.btn_remove', function(){
								var button_id = $(this).attr("id"); 
								$('#row'+button_id+'').remove();
							});
							});


						</script>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php } ?>