<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>

<body>
	<!-- Main navbar -->
				<?php include('includes/nav.php') ?>
				<?php include('includes/connection.php') ?>

	<!-- /main navbar -->


	<!-- Page container -->
					<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-md-8">

							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add New Volunteer</h5>
								</div>
								 <div id="alert_message"></div>

								<div class="panel-body">
									<form class="form-horizontal"   enctype='multipart/form-data' method="post" id='addvolunteer' action="#">

									<div class="form-group">
											<label class="control-label col-lg-2">Volunteer Name</label>
											<div class="col-lg-10">
												<input type="text" name="name" required="" class="form-control">
											</div>
									</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Volunteer Salary</label>
											<div class="col-lg-10">
												<input type="text" name="salary" required="" class="form-control">
											</div>
									</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Date Of Joining</label>
											<div class="col-lg-10">
												<input type="date" name="doj" required="" class="form-control">
											</div>
									</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Area Of Service</label>
											<div class="col-lg-10">
												<input type="text" name="areaofservice" required="" class="form-control">
											</div>
									</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Area Code</label>
											<div class="col-lg-10">
												<input type="number" name="areacode" required="" class="form-control">
											</div>
									</div>
									 <div class="form-group" id="hideshow" style="display: none">
				                          	<label class="control-label col-lg-2">&nbsp;</label>
				                          	 <img class="img img-responsive"   id="uploadPreview"  height="100" width="150" class="img-thumb-modal">
				                          	</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Picture</label>
											<div class="col-lg-10">
												<input type="file" name="picture" id="uploadImage" onchange="PreviewImage('uploadImage','uploadPreview')" required="" class="form-control btn btn-default">
											</div>
									</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Email</label>
											<div class="col-lg-10">
												<input type="email" name="email" required="" class="form-control">
											</div>
									</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Profile , bio data</label>
											<div class="col-lg-10">
												<textarea name="biodata" required="" class="form-control"></textarea>
											</div>
									</div>
									<div class="form-group">
										<label  class="control-label col-lg-2">Upload</label>
											<label>Cv /Document</label>
											<div class="col-lg-10">
												<input type="file"   name="cv" required="" class="form-control btn btn-default">
											</div>
										</div>
									<div class="form-group">
											<label class="control-label col-lg-2">Phone</label>
											<div class="col-lg-10">
												<input type="text" name="phone" required="" class="form-control">
											</div>
									</div>	
									<div class="form-group">
				                        	<label class="control-label col-lg-2">Designations, reporting </label>
				                        	<div class="col-lg-10">
					                            <input type="text" class="form-control" required="" name="designation">
				                            </div>
				                        </div>
									<div class="form-group">
				                        	<label class="control-label col-lg-2">City</label>
				                        	<div class="col-lg-10">
					                            <select name="city" required="" class="form-control select-multiple-tokenization">
					                              	 <option label="Select City"></option>
				           						<?php
				            $select_cities=mysqli_query($con,"Select * from cities");
				                while($fetch_cities =mysqli_fetch_array($select_cities)){
				           		echo" <option value='".$fetch_cities['id']."'>".$fetch_cities['city']."</option>";
					                                } ?>
					                            </select>
				                            </div>
				                        </div>
				                    <div class="form-group">
											<label class="control-label col-lg-2">Address</label>
											<div class="col-lg-10">
												<textarea name="address" required="" class="form-control"></textarea> 
											</div>
									</div>	    
				                        <div class="text-right">
											<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
						<div class="col-lg-2"></div>
					</div>
				
					



					<!-- Footer -->
					<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
	    function PreviewImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);
            $('#hideshow').css('display', 'block');

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }

	  $("#addvolunteer").on('submit', function(e) {
              e.preventDefault();
               $.ajax({
                   url: 'Functions/Insert.php?form=addvolunteer',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
              $('#alert_message').html(result);
                    }
                })
               
                
            }); 
	
</script>
</body>
</html>
<?php } ?>