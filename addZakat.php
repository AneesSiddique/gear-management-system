
<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?><body>
	<!-- Main navbar -->
<?php include('includes/nav.php') ?>
<?php include('includes/connection.php') ?>
	<!-- /main navbar -->



<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-md-8">

							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Self Employment Zakat</h5>
								</div>
								   <div id="alert_message"></div>

								<div class="panel-body">
<form class="form-horizontal" enctype='multipart/form-data' method="post" id='addzakat' action="#">


									<div class="form-group">
											<label class="control-label col-lg-2">Amount</label>
											<div class="col-lg-10">
												<input type="text" name="amount" required=""  class="form-control">
											</div>
										</div>	

										
									
										<div class="form-group">
											<label class="control-label col-lg-2">Partner Assessment </label>
											<div class="col-lg-10">
											<input type="text" class="form-control" name="partner_assesment">
											</div>
										</div>
										 <div class="form-group" id="hideshow" style="display: none">
				                          	<label class="control-label col-lg-2">&nbsp;</label>
				                          	 <img class="img img-responsive"   id="uploadPreviewScannedImages"  height="100" width="150" class="img-thumb-modal">
				                          	</div>
										<div class="form-group">
									<label class="control-label col-lg-2">Amount paid Proof</label>
											<label>Upload Scanned Images</label>
											<div class="col-lg-10">
												<input type="file"  id="uploadImage" onchange="uploadPreviewScannedImage('uploadImage','uploadPreviewScannedImages')" name="amountproof" required="" class="form-control btn btn-default">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Volunteer select </label>
											<div class="col-lg-10">
												<select name="volunteers" required="" class="select-multiple-tokenization" >					
											<option label="Select Refrence"></option>
				           						<?php
				            $select_volunteers=mysqli_query($con,"Select * from volunteers");
				                while($fetch_volunteers =mysqli_fetch_array($select_volunteers)){
				           		echo" <option value='".$fetch_volunteers['id']."'>".$fetch_volunteers['name']."</option>";
					                                } ?>
										</select>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Volunteer Comments</label>
											<div class="col-lg-10">
												<input type="text" required="" name="volcomments" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Date</label>
											<div class="col-lg-10">
												<input type="date" name="date" required="" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-lg-2">Area</label>
											<div class="col-lg-10">
												<input type="text" name="area" required="" class="form-control">
											</div>
										</div>	
										<div class="form-group" id="hideshow2" style="display: none">
				                          	<label class="control-label col-lg-2">&nbsp;</label>
				                          	 <img class="img img-responsive"   id="uploadPreviewduringImages"  height="100" width="150" class="img-thumb-modal">
				                          	</div>
										<div class="form-group">
									<label class="control-label col-lg-2">Upload</label>
											<label>Upload Pictures during the project 
</label>
											<div class="col-lg-10">
												<input type="file"  id="uploadImageduring" onchange="PreviewImageduringpro('uploadImageduring','uploadPreviewduringImages')" name="uploadduringproject" required="" class="form-control btn btn-default">
											</div>
										</div>
										<div class="form-group" id="hideshow3" style="display: none">
				                          	<label class="control-label col-lg-2">&nbsp;</label>
				                          	 <img class="img img-responsive"   id="uploadPreviewfinalImages"  height="100" width="150" class="img-thumb-modal">
				                          	</div>
										<div class="form-group">
									<label class="control-label col-lg-2">Upload</label>
											<label>Upload Final Setup Pictures, 
 
</label>
											<div class="col-lg-10">
												<input type="file" id="uploadImagefinal" onchange="PreviewImagefinalsetup('uploadImagefinal','uploadPreviewfinalImages')" name="uploadfinalsetup" required="" class="form-control btn btn-default">
											</div>
										</div>		
									<div class="form-group">
											<label class="control-label col-lg-2">Business Name</label>
											<div class="col-lg-10">
												<input type="text" name="bussiness" class="form-control">
											</div>
										</div>	
									<div class="form-group">
										 
											<label class="control-label col-lg-2">Business </label>
											<label>select from category</label>
											<div class="col-lg-10">
												 <select name="bussiness_categories" id="bussiness_categories" required=""  class="select-multiple-tokenization">
				                        		 	 <option label="Select Refrence"></option>
				           						<?php
				            $select_categories=mysqli_query($con,"Select * from bussiness_categories");
				                while($fetch_categories =mysqli_fetch_array($select_categories)){
				           		echo" <option value='".$fetch_categories['id']."'>".$fetch_categories['category']."</option>";
					                                } ?>
					                            </select>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-lg-2">Income forecast </label>
											<label>value we added to the society</label>
											<div class="col-lg-10">
												<input type="text" name="value" required="" class="form-control">
											</div>
										</div>	
									
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
						<div class="col-lg-2"></div>
					</div>
				
					



					<!-- Footer -->
						<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
	 function uploadPreviewScannedImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);
            $('#hideshow').css('display', 'block');

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
         function PreviewImageduringpro(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);
            $('#hideshow2').css('display', 'block');

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
         function PreviewImagefinalsetup(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);
            $('#hideshow3').css('display', 'block');

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
	  $("#addzakat").on('submit', function(e) {
              e.preventDefault();
               $.ajax({
                   url: 'Functions/Insert.php?form=addzakat',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
              $('#alert_message').html(result);
                    }
                })
               
                
            }); 
	
</script>
</body>
</html>
<?php } ?>