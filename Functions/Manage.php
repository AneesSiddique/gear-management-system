<?php
include('../includes/connection.php');
if (isset($_POST['donation']) == 'managedonation') {
	$manage_Donation_Query = "SELECT donations.id,donations.amount,donations.paymentmode,donations.number,donations.scannedimg,donations.donatedate,donors.name,donors.profile,types.title from donations LEFT OUTER JOIN donors ON donations.donorid=donors.id LEFT OUTER JOIN types ON donations.type=types.id";
	$manage_Donation_Execute = mysqli_query($con,$manage_Donation_Query);
	
	if (mysqli_num_rows($manage_Donation_Execute) > 0) {
		
		$data = array();
		while ($manage_Donation_Data = mysqli_fetch_array($manage_Donation_Execute)) {
			$data[] = $manage_Donation_Data;
		}
		echo json_encode($data);
	}
	
}
if (isset($_POST['donor']) == 'managedonor') {
	$manage_Donor_Query = "SELECT donors.id,donors.name,donors.email,donors.phone,donors.designation,donors.profile,donors.about,donors.address,donors.companydetail,professions.profession,cities.city,volunteers.name as'volunteername' from donors LEFT OUTER JOIN professions ON donors.professionid=professions.id LEFT OUTER JOIN cities ON donors.cityid=cities.id LEFT OUTER JOIN volunteers ON donors.refrenceid=volunteers.id";
	$manage_Donor_Execute = mysqli_query($con,$manage_Donor_Query);
	
	if (mysqli_num_rows($manage_Donor_Execute) > 0) {
		
		$data = array();
		while ($manage_Donor_Data = mysqli_fetch_array($manage_Donor_Execute)) {
			$data[] = $manage_Donor_Data;
		}
		echo json_encode($data);
	}

}
if (isset($_POST['volunteer']) == 'managevolunteer') {
	$manage_Volunteer_Query = "SELECT * from volunteers LEFT OUTER JOIN cities ON volunteers.cityid=cities.id ";
	$manage_Volunteer_Execute = mysqli_query($con,$manage_Volunteer_Query);
	
	if (mysqli_num_rows($manage_Volunteer_Execute) > 0) {
		
		$data = array();
		while ($manage_Volunteer_Data = mysqli_fetch_array($manage_Volunteer_Execute)) {
			$data[] = $manage_Volunteer_Data;
		}
		echo json_encode($data);
	}

}