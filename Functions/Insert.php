<?php
include('../includes/connection.php');
		// Add Donation 
       if($_GET['form']=='adddonation'){
		$select = htmlspecialchars($_POST['select']);
		$amount = htmlspecialchars($_POST['amount']);
		$selectpaymentmode = htmlspecialchars($_POST['selectpaymentmode']);
		
		$donationdate = htmlspecialchars($_POST['donationdate']);
		$selectdonor = htmlspecialchars($_POST['selectdonor']);
$Cheque_number = htmlspecialchars($_POST['number']);
$bank_number = htmlspecialchars($_POST['numbertwo']);
$other_number = htmlspecialchars($_POST['numberthree']);
	$OtherPaymentName = htmlspecialchars($_POST['OtherPaymentName']);
		if($selectpaymentmode=="other"){
			$selectpaymentmode =$OtherPaymentName;
		}
		$u_image =$_FILES['scannedimg'];
		$imagefiletype=$_FILES['scannedimg']['type'];
		$image_tmp =$_FILES['scannedimg']['tmp_name'];
		$image_size = $_FILES['scannedimg']['size'];
		$f_extension = explode('/',$imagefiletype);
		 $f_extension = strtolower(end($f_extension));
		$f_newfile = uniqid().'.'.$f_extension;
		$store ="../assets/img/donations/scannedimg/".$f_newfile;
		if($amount=="" || $selectpaymentmode=="" || $donationdate=="" || $selectdonor=="" || $f_extension==""){

	echo "<div class='alert alert-danger'>$amount Any Field Is Empty!</div>";

		}else{
	
	if($f_extension=='png' || $f_extension=='jpg' ||$f_extension=='jpeg' || $f_extension=='gif'   ){
		$var = move_uploaded_file($image_tmp,"$store");
		$insert = mysqli_query($con,"insert into donations (type,amount,paymentmode,donatedate,donorid,scannedimg,number,numbertwo,numberthree) values('$select','$amount','$selectpaymentmode','$donationdate','$selectdonor','$f_newfile','$Cheque_number','$bank_number','$other_number')");
		if($insert){
			
			echo"<script>
			$('#adddonation').trigger('reset');
				$('#hideshow').css('display', 'none');
			</script>";
		
echo "<div class='alert alert-success'>Successfully Inserted!</div>";

		}
		}
		else{
			echo "<div class='alert alert-danger'>This Is Incorrect File Please Select Image File!</div>";
		}}}
		//End Add Donation

		// Add Donor
  if($_GET['form']=='adddonor'){

$name = htmlspecialchars($_POST['name']);
$email = htmlspecialchars($_POST['email']);
$phone = htmlspecialchars($_POST['phone']);
$designation = htmlspecialchars($_POST['designation']);
$refrence = htmlspecialchars($_POST['refrence']);
$howhegottoknowabout = htmlspecialchars($_POST['howhegottoknowabout']);
$address = htmlspecialchars($_POST['address']);
$companydetails = htmlspecialchars($_POST['companydetails']);
$profession = htmlspecialchars($_POST['profession']);
$city = htmlspecialchars($_POST['city']);
        $u_image =$_FILES['profile'];
		$imagefiletype=$_FILES['profile']['type'];
		$image_tmp =$_FILES['profile']['tmp_name'];
		$image_size = $_FILES['profile']['size'];
		$f_extension = explode('/',$imagefiletype);
		 $f_extension = strtolower(end($f_extension));
		$f_newfile = uniqid().'.'.$f_extension;
		$store ="../assets/img/donors/profile/".$f_newfile;
		if($name=="" || $email=="" || $phone=="" || $designation=="" || $refrence=="" || $howhegottoknowabout=="" || $address=="" || $companydetails=="" || $profession=="" || $city==""){
			echo "<div class='alert alert-danger'>Any Field Is Empty !</div>";
		}
else{	
	if($f_extension=='png' || $f_extension=='jpg' ||$f_extension=='jpeg' || $f_extension=='gif'   ){
		
		$insert = mysqli_query($con,"insert into donors (name,email,phone,designation,refrenceid,profile,about,address,companydetail,professionid,cityid) values('$name','$email','$phone','$designation','$refrence','$f_newfile','$howhegottoknowabout','$address','$companydetails','$profession','$city')");
		if($insert){
			$var = move_uploaded_file($image_tmp,"$store");
			echo"<script>$('#adddonor').trigger('reset');
			  $('#hideshow').css('display', 'none');

			</script>";
		
echo "<div class='alert alert-success'>Successfully Inserted!</div>";

		}
		}
		else{
			echo "<div class='alert alert-danger'>This Is Incorrect File Please Select Image File!</div>";
		}  }}
 		//EnD Donor

 		// Add Volunteer
 		  if($_GET['form']=='addvolunteer'){
 		  	$name = htmlspecialchars($_POST['name']);
 		  	$doj = htmlspecialchars($_POST['doj']);
 		  	$areaofservice = htmlspecialchars($_POST['areaofservice']);
 		  	$areacode = htmlspecialchars($_POST['areacode']);
 		   	$email = htmlspecialchars($_POST['email']);
 		  	$biodata = htmlspecialchars($_POST['biodata']);
 		  	$phone = htmlspecialchars($_POST['phone']);
 		  	$designation = htmlspecialchars($_POST['designation']);
 		  	$city = htmlspecialchars($_POST['city']);
 		  	$address = htmlspecialchars($_POST['address']);
 		  	$salary = htmlspecialchars($_POST['salary']);
  			$u_image =$_FILES['picture'];
			$imagefiletype=$_FILES['picture']['type'];
			$image_tmp =$_FILES['picture']['tmp_name'];
			$image_size = $_FILES['picture']['size'];
			$f_extension = explode('/',$imagefiletype);
		 	$f_extension = strtolower(end($f_extension));
			$f_newfile = uniqid().'.'.$f_extension;
			$store ="../assets/img/Volunteers/profile/".$f_newfile;

	        $u_file =$_FILES['cv'];
			$filetype=$_FILES['cv']['type'];
			$file_tmp =$_FILES['cv']['tmp_name'];
			$file_size = $_FILES['cv']['size'];
			$extension = end(explode(".", $_FILES["cv"]["name"]));
			$file_newfile = uniqid().'.'.$extension;
			$store_file ="../assets/img/Volunteers/documents/".$file_newfile;
			if($name=="" || $doj=="" || $areaofservice=="" || $areacode=="" || $email=="" || $biodata=="" || $phone=="" || $designation=="" || $city=="" || $address=="" || $salary=="" || $f_extension=="" || $file_newfile==""){

					echo "<div class='alert alert-danger'>Any Field is Empty!</div>";	

			}else{

if($f_extension=='png' || $f_extension=='jpg' ||$f_extension=='jpeg' || $f_extension=='gif'   ){
	}
	else{
	echo "<div class='alert alert-danger'>This Is Incorrect File Please Select Image File!</div>";	
		}
	if( $extension=='pdf' ||$extension=='docx' || $extension=='doc'   ){
		
	}
	else{
			echo "<div class='alert alert-danger'> This Is Incorrect File Please Select PDF Or Word File!</div>";
		}
if($f_extension=='png' || $f_extension=='jpg' ||$f_extension=='jpeg' || $f_extension=='gif' && $extension=='pdf' ||$extension=='docx' || $extension=='doc'   ){
	$insert = mysqli_query($con,"insert into volunteers (name,joiningdate,servicearea,areacode,picture,email,biodata,cv,phone,designation,cityid,address,salary) values('$name','$doj','$areaofservice','$areacode','$f_newfile','$email','$biodata','$file_newfile','$phone','$designation','$city','$address','$salary')");
		if($insert){
			
			$var = move_uploaded_file($image_tmp,"$store");
			$varupload = move_uploaded_file($file_tmp,"$store_file");
			echo"<script>$('#addvolunteer').trigger('reset');
			 $('#hideshow').css('display', 'none');</script>";
		
echo "<div class='alert alert-success'>Successfully Inserted!</div>";

		}}

}

 		  }
 		
 		// End Volunteer
 		  //Add Zakat
 		   if($_GET['form']=='addzakat'){

$amount = htmlspecialchars($_POST['amount']);
$partner_assesment = htmlspecialchars($_POST['partner_assesment']);
$volunteers = htmlspecialchars($_POST['volunteers']);
$volcomments = htmlspecialchars($_POST['volcomments']);
$date = htmlspecialchars($_POST['date']);
$area = htmlspecialchars($_POST['area']);
$bussiness = htmlspecialchars($_POST['bussiness']);
$bussiness_categories = htmlspecialchars($_POST['bussiness_categories']);
$value = htmlspecialchars($_POST['value']);
$amount_file =$_FILES['amountproof'];
$amounttype=$_FILES['amountproof']['type'];
$amount_tmp =$_FILES['amountproof']['tmp_name'];
$amount_size = $_FILES['amountproof']['size'];
$extension_amount = end(explode(".", $_FILES["amountproof"]["name"]));
$amount_newfile = uniqid().'.'.$extension_amount;
$store_amount ="../assets/img/Zakat/AmountProof/".$amount_newfile;


$upload_during_file =$_FILES['uploadduringproject'];
$upload_duringtype=$_FILES['uploadduringproject']['type'];
$upload_during_tmp =$_FILES['uploadduringproject']['tmp_name'];
$upload_during_size = $_FILES['uploadduringproject']['size'];
$extension_upload_during = end(explode(".", $_FILES["uploadduringproject"]["name"]));
$upload_during_newfile = uniqid().'.'.$extension_upload_during;
$store_upload_during ="../assets/img/Zakat/UploadDuringProject/".$upload_during_newfile;

$finalsetup_file =$_FILES['uploadfinalsetup'];
$finalsetuptype=$_FILES['uploadfinalsetup']['type'];
$finalsetup_tmp =$_FILES['uploadfinalsetup']['tmp_name'];
$finalsetup_size = $_FILES['uploadfinalsetup']['size'];
$extension_finalsetup = end(explode(".", $_FILES["uploadfinalsetup"]["name"]));
$finalsetup_newfile = uniqid().'.'.$extension_finalsetup;
$store_upload_final ="../assets/img/Zakat/UploadFinalSetup/".$finalsetup_newfile;

if($amount=="" || $partner_assesment=="" || $volunteers=="" || $volcomments=="" || $date=="" || $area=="" || $bussiness=="" || $bussiness_categories=="" || $value=="" || $extension_amount=="" || $extension_upload_during=="" || $extension_finalsetup==""){
	echo "<div class='alert alert-danger'>Any Field Is Empty !</div>";

}
else{
if($extension_amount=='png' || $extension_amount=='jpg' ||$extension_amount=='jpeg' || $extension_amount=='gif'  AND $extension_upload_during=='png' || $extension_upload_during=='jpg' ||$extension_upload_during=='jpeg' || $extension_upload_during=='gif' AND $extension_finalsetup=='png' || $extension_finalsetup=='jpg' ||$extension_finalsetup=='jpeg' || $extension_finalsetup=='gif'  ){
		
		$insert = mysqli_query($con,"insert into self_employments (amount,partnerid,amountproof,volunteerid,volunteercomments,date,area,picture_during_project,final_setup_picture,businessname,businesscategory,incomeforecast,amountype) values('$amount','$partner_assesment','$amount_newfile','$volunteers','$volcomments','$date','$area','$upload_during_newfile','$finalsetup_newfile','$bussiness','$bussiness_categories','$value','1')");
		if($insert){
			$movedirectoryamount = move_uploaded_file($amount_tmp,"$store_amount");
			$movedirectorydurig = move_uploaded_file($upload_during_tmp,"$store_upload_during");
			$movedirectoryfinal = move_uploaded_file($finalsetup_tmp,"$store_upload_final");
			echo"<script>$('#addzakat').trigger('reset');
				$('#hideshow').css('display', 'none');
			$('#hideshow2').css('display', 'none');
			$('#hideshow3').css('display', 'none');

			</script>";



				echo "<div class='alert alert-success'>Successfully Inserted!</div>";

		}
		else{
	echo "<div class='alert alert-danger'>Something Error !</div>";

		}


		}
		else{

				echo "<div class='alert alert-danger'>Please Select Correct Image Formate !</div>";

		}

}

 		   } 
//Add Sadaqah
 		    		   if($_GET['form']=='addsadaqah'){
$amount = htmlspecialchars($_POST['amount']);
$partner_assesment = htmlspecialchars($_POST['partner_assesment']);
$volunteers = htmlspecialchars($_POST['volunteers']);
$volcomments = htmlspecialchars($_POST['volcomments']);
$date = htmlspecialchars($_POST['date']);
$area = htmlspecialchars($_POST['area']);
$permonth = htmlspecialchars($_POST['permonth']);
$bussiness = htmlspecialchars($_POST['bussiness']);
$bussiness_categories = htmlspecialchars($_POST['bussiness_categories']);
$value = htmlspecialchars($_POST['value']);
$amount_file =$_FILES['amountproof'];
$amounttype=$_FILES['amountproof']['type'];
$amount_tmp =$_FILES['amountproof']['tmp_name'];
$amount_size = $_FILES['amountproof']['size'];
$extension_amount = end(explode(".", $_FILES["amountproof"]["name"]));
$amount_newfile = uniqid().'.'.$extension_amount;
$store_amount ="../assets/img/Sadaqah/AmountProof/".$amount_newfile;


$upload_during_file =$_FILES['uploadduringproject'];
$upload_duringtype=$_FILES['uploadduringproject']['type'];
$upload_during_tmp =$_FILES['uploadduringproject']['tmp_name'];
$upload_during_size = $_FILES['uploadduringproject']['size'];
$extension_upload_during = end(explode(".", $_FILES["uploadduringproject"]["name"]));
$upload_during_newfile = uniqid().'.'.$extension_upload_during;
$store_upload_during ="../assets/img/Sadaqah/UploadDuringProject/".$upload_during_newfile;

$finalsetup_file =$_FILES['uploadfinalsetup'];
$finalsetuptype=$_FILES['uploadfinalsetup']['type'];
$finalsetup_tmp =$_FILES['uploadfinalsetup']['tmp_name'];
$finalsetup_size = $_FILES['uploadfinalsetup']['size'];
$extension_finalsetup = end(explode(".", $_FILES["uploadfinalsetup"]["name"]));
$finalsetup_newfile = uniqid().'.'.$extension_finalsetup;
$store_upload_final ="../assets/img/Sadaqah/UploadFinalSetup/".$finalsetup_newfile;

if($amount=="" || $partner_assesment=="" || $volunteers=="" || $volcomments=="" || $date=="" || $area=="" || $bussiness=="" || $bussiness_categories=="" || $value=="" || $extension_amount=="" || $extension_upload_during=="" || $extension_finalsetup==""){
	echo "<div class='alert alert-danger'>Any Field Is Empty !</div>";

}
else{
if($extension_amount=='png' || $extension_amount=='jpg' ||$extension_amount=='jpeg' || $extension_amount=='gif'  AND $extension_upload_during=='png' || $extension_upload_during=='jpg' ||$extension_upload_during=='jpeg' || $extension_upload_during=='gif' AND $extension_finalsetup=='png' || $extension_finalsetup=='jpg' ||$extension_finalsetup=='jpeg' || $extension_finalsetup=='gif'  ){
		
		$insert = mysqli_query($con,"insert into self_employments (amount,partnerid,amountproof,volunteerid,volunteercomments,date,area,picture_during_project,final_setup_picture,businessname,businesscategory,incomeforecast,amountype,permonth) values('$amount','$partner_assesment','$amount_newfile','$volunteers','$volcomments','$date','$area','$upload_during_newfile','$finalsetup_newfile','$bussiness','$bussiness_categories','$value','2','$permonth')");
		if($insert){
			$movedirectoryamount = move_uploaded_file($amount_tmp,"$store_amount");
			$movedirectorydurig = move_uploaded_file($upload_during_tmp,"$store_upload_during");
			$movedirectoryfinal = move_uploaded_file($finalsetup_tmp,"$store_upload_final");
			echo"<script>$('#addsadaqah').trigger('reset');
			$('#hideshow').css('display', 'none');
			$('#hideshow2').css('display', 'none');
			$('#hideshow3').css('display', 'none');
		

			</script>";



				echo "<div class='alert alert-success'>Successfully Inserted!</div>";

		}
		else{
	echo "<div class='alert alert-danger'>Something Error !</div>";

		}


		}
		else{

				echo "<div class='alert alert-danger'>Please Select Correct Image Formate !</div>";

		}

}

 		   } 
if($_GET['form']=='addnewaccount'){
$volunteer = htmlspecialchars($_POST['volunteer']);
$expensetype = htmlspecialchars($_POST['expensetype']);
$amount = htmlspecialchars($_POST['amount']);
$paymentmode = htmlspecialchars($_POST['paymentmode']);
$Cheque_number = htmlspecialchars($_POST['number']);
$bank_number = htmlspecialchars($_POST['numbertwo']);
$other_number = htmlspecialchars($_POST['numberthree']);


	$OtherPaymentName = htmlspecialchars($_POST['OtherPaymentName']);
		if($paymentmode=="other"){
			$paymentmode =$OtherPaymentName;
		}
$amount_file =$_FILES['proof'];
$amounttype=$_FILES['proof']['type'];
$amount_tmp =$_FILES['proof']['tmp_name'];
$amount_size = $_FILES['proof']['size'];
$extension_amount = end(explode(".", $_FILES["proof"]["name"]));
$amount_newfile = uniqid().'.'.$extension_amount;
$store_amount ="../assets/img/Accounts/ExpenseProof/".$amount_newfile;
if($extension_amount=='png' || $extension_amount=='jpg' ||$extension_amount=='jpeg' || $extension_amount=='gif'   ){
	$insert = mysqli_query($con,"insert into expensis (volunteer_id,expensis_type,amount,payment_mode,proof_img,number,numbertwo,numberthree) values('$volunteer','$expensetype','$amount','$paymentmode','$amount_newfile','$Cheque_number','$bank_number','$other_number')");
		if($insert){
			$movedirectoryamount = move_uploaded_file($amount_tmp,"$store_amount");
			echo"<script>$('#addnewaccount').trigger('reset');

			 $('#hideshow').css('display', 'none');</script>";
				echo "<div class='alert alert-success'>Successfully Inserted!</div>";
		}
		else{
	echo "<div class='alert alert-danger'>Something Error !</div>";

		}

	}
	else{
	echo "<div class='alert alert-danger'>This Is Incorrect File Please Select Image File!</div>";	
		}
}


 		// Add Assessment 
 			
 		  	if ($_GET['form']=='partnerAssessment') {
 		  		$application_Holder = $_POST['application_Holder'];
 		  		$cnic_No = $_POST['cnic_No'];
 		  		$gender = @$_POST['gender'];
 		  		$contact_No = $_POST['contact_No'];
 		  		$qualification = $_POST['qualification'];
 		  		$current_Address = $_POST['current_Address'];
 		  		$Address = $_POST['Address'];
 		  		$marital_status = $_POST['marital_status'];
 		  		$total_people = $_POST['total_people'];
 		  		$total_income = $_POST['total_income'];
 		  		$total_kids = $_POST['total_kids'];
 		  		$why_Need_Loan = $_POST['why_Need_Loan'];
 		  		$problum_Facing_Education = $_POST['problum_Facing_Education'];
 		  		$required = $_POST['required'];
 		  		$amount = $_POST['amount'];
 		  		$amount_In_Words = $_POST['amount_In_Words'];
 		  		$pay_time = $_POST['pay_time'];
 		  		$monthly_Stallment = $_POST['monthly_Stallment'];
 		  		$cnic_back = $_FILES['cnic_back'];
 		  		$cnic_Front = $_FILES['cnic_Front'];
 		  		$form_back = $_FILES['form_back'];
 		  		$form_Front = $_FILES['form_Front'];
 		  		$imagefiletype=$_FILES['cnic_back']['type'];
				$image_tmp_back =$_FILES['cnic_back']['tmp_name'];
				$image_size = $_FILES['cnic_back']['size'];
				$f_extension = explode('/',$imagefiletype);
				 $f_extension = strtolower(end($f_extension));
				$f_newfile = uniqid().'.'.$f_extension;
				$cnic_back_store ="../assets/img/partner_assessment/cnic_back/".$f_newfile;
				$imagefiletypefront=$_FILES['cnic_Front']['type'];
				$image_tmp_front =$_FILES['cnic_Front']['tmp_name'];
				$image_size_front = $_FILES['cnic_Front']['size'];
				$f_extension_front = explode('/',$imagefiletypefront);
				 $f_extension_front = strtolower(end($f_extension_front));
				$f_newfile_front = uniqid().'.'.$f_extension_front;
				$cnic_front_store ="../assets/img/partner_assessment/cnic_front/".$f_newfile_front;
				$imagefiletypeform_b=$_FILES['form_back']['type'];
				$image_tmp_form_b =$_FILES['form_back']['tmp_name'];
				$image_size_from_b = $_FILES['form_back']['size'];
				$f_extension_from_b = explode('/',$imagefiletypeform_b);
				 $f_extension_from_b = strtolower(end($f_extension_from_b));
				$f_newfile_from_b = uniqid().'.'.$f_extension_front;
				$cnic_from_b_store ="../assets/img/partner_assessment/form_back/".$f_newfile_from_b;
				$imagefiletypeform_f=$_FILES['form_Front']['type'];
				$image_tmp_form_f =$_FILES['form_Front']['tmp_name'];
				$image_size_from_f = $_FILES['form_Front']['size'];
				$f_extension_from_f = explode('/',$imagefiletypeform_f);
				 $f_extension_from_f = strtolower(end($f_extension_from_f));
				$f_newfile_from_f = uniqid().'.'.$f_extension_front;
				$cnic_from_f_store ="../assets/img/partner_assessment/form_front/".$f_newfile_from_f;

				if($f_extension=='png' || $f_extension=='jpg' ||$f_extension=='jpeg' || $f_extension=='gif'   ){
				$var = move_uploaded_file($image_tmp_back,"$cnic_back_store");
				$var = move_uploaded_file($image_tmp_front,"$cnic_front_store");
				$var = move_uploaded_file($image_tmp_form_b,"$cnic_from_b_store");
				$var = move_uploaded_file($image_tmp_form_f,"$cnic_from_f_store");
		
		$insert = mysqli_query($con,"insert into partners (applicationholder,cnic_number,gender,phone,address,home_status,education,cnic_back,cnic_front,maritialstatus,total_people,total_income,total_children,educationproblem,	why_need_loan) values('$application_Holder','$cnic_No','$gender','$contact_No','$qualification','$current_Address','$Address','$cnic_back_store','$cnic_front_store','$marital_status','$total_people','$total_income','$total_kids','$problum_Facing_Education','$why_Need_Loan')");
		if($insert){
			$last_id = mysqli_insert_id($con);
			mysqli_query($con,"insert into assessment_loan_details(loantype,amount,put_amount_in_words,loan_pay_time,monthlystalments,partnerid) values('$required','$amount','$amount_In_Words','$pay_time','$monthly_Stallment','$last_id')");

			mysqli_query($con,"insert into form_images(front_image,back_image,partnerid) values ('$cnic_from_f_store','$cnic_from_b_store','$last_id')");

			foreach($_POST['user']['person_name'] as $index=>$pn){
					// array_push($data, array('name'=>$pn,'work_relation'=>$_POST['user']['relation_with'][$index],'work_nature'=>$_POST['user']['work_nature'][$index]));
					$name = $_POST['user']['person_name'][$index];
					$relation = $_POST['user']['relation_with'][$index];
					$work = $_POST['user']['work_nature'][$index];
					$income = $_POST['user']['monthly_income'][$index];
					$family_expensis = $_POST['user']['monthly_family_expensis'][$index];
					$how_work = $_POST['user']['how_work'][$index];
					$expected_amount = $_POST['user']['expected_amount'][$index];
		$insert = mysqli_query($con,"insert into income_details (name,relation,nature_of_work,monthly_income,monthy_family_expensis,how_work_start,	axpected_monthly_amount,partnerid) values('$name','$relation','$work','$income','$family_expensis','$how_work','$expected_amount','$last_id')");
					
				}

				foreach ($_POST['children']['child_Name']  as $key => $value) {
							$n = $_POST['children']['child_Name'][$key];
							$o = $_POST['children']['child_Age'][$key];
							$s = $_POST['children']['school_Class_Name'][$key];
							$insert = mysqli_query($con,"insert into children (name,age,class,partnerid) values('$n','$o','$s','$last_id')");
						}
			echo "<div class='alert alert-success'>Successfully Inserted!</div>";
			echo"<script>$('#partner_Assessment')[0].reset();</script>";
		}
		}
 		  		
					
						
 		  	}
// End Assessment 

// Partner Donation
if ($_GET['form']=='partnerDonation') {
	$partner = $_POST['partner_assess'];
	$business_name = $_POST['business_name'];
	$fund = $_POST['fund'];
	$volunteer = $_POST['volunteer'];
	$img = $_FILES['Scanned_img'];
	$imagefiletype=$_FILES['Scanned_img']['type'];
	$image_tmp=$_FILES['Scanned_img']['tmp_name'];
	$image_size = $_FILES['Scanned_img']['size'];
	$f_extension = explode('/',$imagefiletype);
	$f_extension = strtolower(end($f_extension));
	$f_newfile = uniqid().'.'.$f_extension;
	$store ="../assets/img/partner_donation/".$f_newfile;
	if($f_extension=='png' || $f_extension=='jpg' ||$f_extension=='jpeg' || $f_extension=='gif'   ){
				$var = move_uploaded_file($image_tmp,"$store");
				$insert = mysqli_query($con,"insert into partner_donations (fund_required,bussines,scanned_img,volunteerid,partner_assessment_id) values('$fund','$business_name','$store','$volunteer','$partner')");
		if($insert){
			
			echo"<script>$('#partner_Donation').trigger('reset');</script>";
		
echo "<div class='alert alert-success'>Successfully Inserted!</div>";

		}
		}
		else{
			echo "<div class='alert alert-danger'>This Is Incorrect File Please Select Image File!</div>";
		}
		}		


// Partner End	


?>