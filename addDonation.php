<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>
<body>

	<!-- Main navbar -->
	<?php include('includes/nav.php') ?>
	<?php include('includes/connection.php') ?>

	<!-- /main navbar -->
<?php include('includes/sidebar_nav.php') ?>


	<!-- Page container -->
	


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-md-8">

							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add New Donation</h5>
								</div>
                               <div id="alert_message"></div>
								<div class="panel-body">
									<form class="form-horizontal" enctype='multipart/form-data' method="post" id='adddonation' action="#">

									<div class="form-group">
				                        	<label class="control-label col-lg-2">Type</label>
				                        	<div class="col-lg-10">
				                        		 <select name="select" id="type" required=""  class="form-control">
				                        		 	 <option label="Select Type"></option>
				           						<?php
				            $select=mysqli_query($con,"Select * from types");
				                while($fetch =mysqli_fetch_array($select)){
				           		echo" <option value='".$fetch['id']."'>".$fetch['title']."</option>";
					                                } ?>
					                            </select>
				                            </div>
				                        </div>
				                       
										<div class="form-group">
											<label class="control-label col-lg-2">Amount</label>
											<div class="col-lg-10">
												<input type="text" id="amount" required=""  name="amount" class="form-control">
											</div>
										</div>
										<div class="form-group">
				                        	<label class="control-label col-lg-2">Payment Mode</label>
				                        	<div class="col-lg-10">
					                            <select required=""  name="selectpaymentmode" class="form-control paymentmode">
					                            	<option label="Select Payment Mode"></option>
					                                <option value="cash">Cash</option>
					                                <option value="cheque">Cheque</option>
					                                <option value="easypaisa">Easypaisa</option>
					                                <option value="banktransfer">Bank Transfer</option>
					                                <option value="other">Other</option>
					                            </select>
				                            </div>
				                        </div>
				                        <div class="form-group" style="display:none " id="Cheque">
  <label  class="control-label col-lg-2">Number</label>
  <div class="col-lg-10">
Cheque Number
<input type="text"  id="number" name="number" class="form-control">
</div>
</div>
  
<div class="form-group" style="display:none " id="bank">
  <label  class="control-label col-lg-2">Number</label>
  <div class="col-lg-10">
Bank Transfer Number
<input type="text"  id="number" name="number" class="form-control">
</div>
</div>

 <div class="form-group" style="display:none " id="other">
  <label  class="control-label col-lg-2">Number</label>
  <div class="col-lg-10">
  	Other Number
<input type="text"  id="number" name="number" class="form-control">

Other Payment Name
<input type="text"   name="OtherPaymentName" class="form-control">
</div>
</div>
										<div class="form-group">
				                        	<label class="control-label col-lg-2">Donation Date</label>
				                        	<div class="col-lg-10">
				                        	
											
											
												<input type="date" required="" id="date" name="donationdate" class="form-control">
											</div>
										</div>

									<div class="form-group">
				                        	<label class="control-label col-lg-2">Select Donor</label>
				                        	<div class="col-lg-10">
					                            <select required="" id="donor" name="selectdonor" class="select-multiple-tokenization">
					                                <option label="Select Donor"></option>
					                               <?php
				            $select_donors=mysqli_query($con,"Select * from donors");
				                while($fetch_donors =mysqli_fetch_array($select_donors)){
				           		echo" <option value='".$fetch_donors['id']."'>".$fetch_donors['name']."</option>";
					                                } ?>
					                            </select>
				                            </div>
				                        </div>
				                          <div class="form-group" id="hideshow" style="display: none">
				                          	<label class="control-label col-lg-2">&nbsp;</label>
				                          	 <img class="img img-responsive"   id="uploadPreview"  height="100" width="150" class="img-thumb-modal">
				                          	</div>

				                          	<div class="form-group">

				                          	  
				                        	<label class="control-label col-lg-2">Upload scanned image</label>
				                        	<div class="col-lg-10">
				                        										
											
												<input type="file" accept="image/x-png, image/gif, image/jpeg" id="uploadImage" onchange="PreviewImage('uploadImage','uploadPreview')"   required=""  name="scannedimg" class="btn btn-info form-control">
											</div>
										</div>

										

										<div class="text-right">
											
											<input type="submit" name="sub" value="Insert" class="btn btn-primary" name="">
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
						<div class="col-lg-2"></div>
					</div>
				
					



					<!-- Footer -->
					<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
<!-- Image Preview -->
<script type="text/javascript">
    
// Payement Method Select	
$(document).ready(function(){
    $("select.paymentmode").change(function(){
        var selectedpaymentmode = $(".paymentmode option:selected").val();
        if(selectedpaymentmode=='cheque' || selectedpaymentmode=='banktransfer' || selectedpaymentmode=='other'){
         if(selectedpaymentmode=='cheque'){
         $("#Cheque").css('display', 'block');
          $("#bank").css('display', 'none');
           $("#other").css('display', 'none');
         
         } 
         if(selectedpaymentmode=='banktransfer'){
         $("#bank").css('display', 'block');
          $("#Cheque").css('display', 'none');      
           $("#other").css('display', 'none');
              
         }
         if(selectedpaymentmode=='other'){
        $("#bank").css('display', 'none');
          $("#Cheque").css('display', 'none');      
           $("#other").css('display', 'block');
            
         }}
         else{

         	 $("#bank").css('display', 'none');
          $("#Cheque").css('display', 'none');      
           $("#other").css('display', 'none');
               $("#num").css('display', 'none');
         }

       

    });
});
// Ajax throw form submit
	  $("#adddonation").on('submit', function(e) {
              e.preventDefault();
               $.ajax({
                   url: 'Functions/Insert.php?form=adddonation',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
              $('#alert_message').html(result);
                    }
                })
               
                
            }); 

</script>

</html>
<?php } ?>
