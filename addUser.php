<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>
<body>

	<!-- Main navbar -->
			<?php include('includes/nav.php') ?>

	<!-- /main navbar -->


	<!-- Page container -->
				<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="col-md-12">

							<!-- Horizontal form -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add User</h5>
									
			                	</div>

								<div class="panel-body">
									<form class="form-vertical" action="#">
									<div class="row">
										<div class="col-md-5">
											<div class="form-group ">
											<label class="control-label ">FirstName</label>
											<input type="text" class="form-control" placeholder="Firstname">
											</div>
										</div>
										<div class="col-md-1"></div>
										<div class="col-md-5">
											<div class="form-group ">
											<label class="control-label ">LastName</label>
											<input type="text" class="form-control" placeholder="Lastname">
											</div>
										</div>
									</div>
										<div class="row">
											<div class="col-lg-2 col-md-2">
											<div class="form-group ">
											<label class="control-label ">Username</label>
											<input type="text" class="form-control" placeholder="Username">
											</div>
											</div>
											<div class="col-md-1"></div>
											<div class="col-lg-2 col-md-2">
											<div class="form-group ">
											<label class="control-label ">Email</label>
											<input type="email" class="form-control" placeholder="Email">
											</div>
											</div>
											<div class="col-md-1"></div>
											<div class="col-lg-2 ">
											<div class="form-group ">
											<label class="control-label ">Password</label>
											<input type="password" class="form-control" placeholder="Password">
											</div>
											</div>
											<div class="col-md-1"></div>
											<div class="col-lg-2 col-md-2">
											<div class="form-group ">
											<label class="control-label ">User Type</label>
											<select class="form-control">
											<option>select User Type</option>
											</select>
											</div>
											</div>
										</div>
										

										
										

										
										

										<div class="text-right">
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
				
					



					<!-- Footer -->
				<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php } ?>