<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>

<body>

	<!-- Main navbar -->
				<?php include('includes/nav.php') ?>

	<!-- /main navbar -->


	<!-- Page container -->
					<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Manage Accounts<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
							<div class="heading-elements">
								
		                	</div>
						</div>

						

						<div id="DataTables_Table_0_wrapper" class="table-bordered table-responsive dataTables_wrapper no-footer"><div class="datatable-header"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label><span>Search:</span> <input type="search" class="" placeholder="Search..." aria-controls="DataTables_Table_0"></label></div><div class="dataTables_length" id="DataTables_Table_0_length"><label><span>Show:</span> <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="select2-hidden-accessible" tabindex="-1" aria-hidden="true"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-DataTables_Table_0_length-tn-container"><span class="select2-selection__rendered" id="select2-DataTables_Table_0_length-tn-container" title="10">10</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></label></div></div><div class="datatable-scroll-wrap"><div style="position: absolute; height: 1px; width: 0px; overflow: hidden;"><input type="text" tabindex="0"></div><table class="table datatable-key-basic dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="position: relative;">
							<thead>
								<tr role="row">
								
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Head</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Job Title: activate to sort column ascending">Amount</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Job Title: activate to sort column ascending">Payment Mode</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Job Title: activate to sort column ascending">Number</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Job Title: activate to sort column ascending">scanned image</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Job Title: activate to sort column ascending">Date</th>
								
								<th class="text-center sorting_disabled" rowspan="3" colspan="2" aria-label="Actions" style="width: 100px;">Actions</th>
								
								</tr>
							</thead>
							<tbody>
							<tr role="row" class="odd">
									
									<td>head 1</td>
									<td>Rs: 160000</td>
									<td>Rs: Cheque</td>
									<td>2364723-342342-34</td>
									<td> <a class="example-image-link" href="assets/images/slip.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img src="assets/images/slip.jpg" height="50px" width="50px"></a></td>
									<td>23/10/2018</td>
									<td>
										<a href="#" class="label label-warning">Edit</a>
										<a href="#"  onclick='detailModal("1");' class="label label-success">View</a>
										<a href="#" class="label label-info">Delete</a>
									</td>
								</tr>
								<tr role="row" class="odd">
									
									<td>Head 2</td>
									<td>Rs: 50000</td>
									<td>Rs: Tranfer</td>
									<td>743575-45345345-5</td>
									<td><a class="example-image-link" href="assets/images/slip.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img src="assets/images/slip.jpg" height="50px" width="50px"></a></td>
									<td>23/10/2018</td>
									
									<td>
										<a href="#" class="label label-warning">Edit</a>
										<a href="#" onclick='detailModal("1");' class="label label-success">View</a>
										<a href="#" class="label label-info">Delete</a>
									</td>
								</tr>
								</tbody>
						</table></div><div class="datatable-footer"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 10 of 15 entries</div><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">←</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">2</a></span><a class="paginate_button next" aria-controls="DataTables_Table_0" data-dt-idx="3" tabindex="0" id="DataTables_Table_0_next">→</a></div></div></div>
					</div>
				
					



					<!-- Footer -->
					<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
  function detailModal(id){
     var data = {"id" : id};
   
    $.ajax({
      url: 'Modals/ViewDonationDetails.php',
      type: 'POST',
      data: data,
      success:function(data){
        jQuery('body').append(data);
        jQuery('#detailModal').modal('toggle');
      },
      error: function() {
        alert("Something went Wrong");
      }
    });

   }

</script>
</body>
</html>
<?php } ?>