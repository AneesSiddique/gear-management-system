<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>


<body>

	<!-- Main navbar -->
			<?php include('includes/nav.php') ?>

	<!-- /main navbar -->


	<!-- Page container -->
				<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Manage Partner Assessment<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
							<div class="heading-elements">
								
		                	</div>
						</div>

						

						<div id="DataTables_Table_0_wrapper" class="table-bordered table-responsive dataTables_wrapper no-footer"><div class="datatable-header"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label><span>Search:</span> <input type="search" class="" placeholder="Search..." aria-controls="DataTables_Table_0"></label></div><div class="dataTables_length" id="DataTables_Table_0_length"><label><span>Show:</span> <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="select2-hidden-accessible" tabindex="-1" aria-hidden="true"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-DataTables_Table_0_length-tn-container"><span class="select2-selection__rendered" id="select2-DataTables_Table_0_length-tn-container" title="10">10</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></label></div></div><div class="datatable-scroll-wrap"><div style="position: absolute; height: 1px; width: 0px; overflow: hidden;"><input type="text" tabindex="0"></div><table class="table datatable-key-basic dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="position: relative;">
							<thead>
								<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="First Name: activate to sort column descending">Application Holder</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">CNIC NO#</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Gender</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Contact Number</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Current Address</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Partner House Status</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Education</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Cnic Front</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Cnic Back</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Maritial Status </th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Current income to increase to bussiness information</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Why Are Need To You Loan ?</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending">Children Details</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending"> Are You Facing Any Problem To Education in kids ?</th>
								<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending"> Loan/Zakat Information</th>
							
								<th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="Actions" style="width: 100px;">Actions</th>
								
								</tr>
							</thead>
							<tbody>
							<tr role="row" class="odd">
									
									<td> Gafoor</td>
									<td class="sorting_1">150345345-4353-3</td>
								<td>Male</td>
									<td>03423432433</td>
									<td>Address 1</td>
									<td>Owner</td>
									<td>Matric</td>
									
									<td><img src="assets/images/image.png" width="50px" height="50px"></td>
									<td><img src="assets/images/image.png" width="50px" height="50px"></td>
									<td >UnMarried</td>
										<td>10k</td>
										<td>Home Issues</td>
										<td><a href="#" class="label label-info">View Children</a></td>
										<td>No</td>
										<td><a href="#" class="label label-success">View Information</a></td>
									<td>
										<a href="#" class="label label-warning">Edit</a>
										<a href="#" class="label label-danger">Delete</a>
									</td>
								</tr>
								<tr role="row" class="odd">
									
									<td> Basit</td>
									<td class="sorting_1">150345345-4353-3</td>
								<td>Male</td>
									<td>03423432433</td>
									<td>Address 2</td>
									<td>Rent</td>
									<td>Primary</td>
									
									<td><img src="assets/images/image.png" width="50px" height="50px"></td>
									<td><img src="assets/images/image.png" width="50px" height="50px"></td>
									<td >Married</td>
										<td>12k</td>
										<td>Home Issues</td>
										<td><a href="#" class="label label-info">View Children</a></td>
										<td>No</td>
										<td><a href="#" class="label label-success">View Information</a></td>
									<td>
										<a href="#" class="label label-warning">Edit</a>
										<a href="#" class="label label-danger">Delete</a>
									</td>
								</tr>
								</tbody>
						</table></div><div class="datatable-footer"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 10 of 15 entries</div><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">←</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">2</a></span><a class="paginate_button next" aria-controls="DataTables_Table_0" data-dt-idx="3" tabindex="0" id="DataTables_Table_0_next">→</a></div></div></div>
					</div>
				
					



					<!-- Footer -->
						<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php } ?>