<?php 
include('../includes/connection.php');
    $id = $_POST['id'];
    // $id = (int)$id;

   
$d_modal_sql = "SELECT donors.id,donors.name,donors.email,donors.phone,donors.designation,donors.profile,donors.about,donors.address,donors.companydetail,professions.profession,cities.city,volunteers.name as'volunteername' from donors LEFT OUTER JOIN professions ON donors.professionid=professions.id LEFT OUTER JOIN cities ON donors.cityid=cities.id LEFT OUTER JOIN volunteers ON donors.refrenceid=volunteers.id WHERE donors.id=$id";

    $d_modal_query = mysqli_query($con,$d_modal_sql);
    $d_modal_detail = mysqli_fetch_array($d_modal_query);

//     $companyemail = $d_modal_detail['companyemail'];
//     $companyname = $d_modal_detail['companyname'];
//     $companylogo = $d_modal_detail['companylogo'];
//     $companydesc = $d_modal_detail['companydesc'];
//     $companycontact = $d_modal_detail['companycontact'];
//     $npnnumber = $d_modal_detail['npnnumber'];
//     $industry = $d_modal_detail['industry'];
//     $city = $d_modal_detail['city'];
//     $country = $d_modal_detail['country'];
//     $status = $d_modal_detail['status'];

 ?>
 <script type="text/javascript">
    function PreviewImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
 </script>
 <!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
         <h3 class="text-center"> Edit Donor</h3>
      <hr>
      </div>
  <form  method="POST" id="update_Donor" enctype="multipart/form-data" action="#">
     <div class="modal-body">
      <div class="container-fluid">
        <span id="alert_message" class="bg-danger"></span>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Donor Name: </label>
                <input type="hidden" name="txt_id" value="<?= $d_modal_detail['id'];?>">
                <input type="text" name="donor_name" class="form-control" value="<?= $d_modal_detail['name'];?>" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">professions: </label>
                <select  name="professions" class="form-control">
                <?php
                    $select=mysqli_query($con,"Select * from professions");
                        while($fetch =mysqli_fetch_array($select)){
                       echo "<option value = '{$fetch['id']}'";
                      if ($d_modal_detail['profession'] == $fetch['profession'])
                          echo "selected = 'selected'";
                      echo ">{$fetch['profession']}</option>";
                    } ?>
                  </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Email: </label>
                <input type="text" name="donor_email" class="form-control" value="<?= $d_modal_detail['email'];?>" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Phone: </label>
                <input type="text" name="phone" class="form-control" value="<?= $d_modal_detail['phone'];?>" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Designation: </label>
                <input type="text" name="Designation" class="form-control" value="<?= $d_modal_detail['designation'];?>" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Volunteer(Ref): </label>
            
               <select  name="refrence" class="form-control">
                    
                   <?php
                      $select_volunteers=mysqli_query($con,"Select * from volunteers");
                      while($fetch_volunteers =mysqli_fetch_array($select_volunteers)){
                       echo "<option value = '{$fetch_volunteers['id']}'";
                      if ($d_modal_detail['volunteername'] == $fetch_volunteers['name'])
                          echo "selected = 'selected'";
                      echo ">{$fetch_volunteers['name']}</option>";
                    } ?>
                </select>
              </div>
            </div>  
             <div class="col-md-6">
              <div class="form-group">
                <label for="size">City: </label>
                     <select  name="city" class="form-control">
                   <?php
                      $select_cities=mysqli_query($con,"Select * from cities");
                      while($fetch_cities =mysqli_fetch_array($select_cities)){
                       echo "<option value = '{$fetch_cities['id']}'";
                      if ($d_modal_detail['city'] == $fetch_cities['city'])
                          echo "selected = 'selected'";
                      echo ">{$fetch_cities['city']}</option>";
                    } ?>
                </select>
              </div>
            </div> 
           
           
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">About: </label>
                <textarea name="about" class="form-control" ><?= $d_modal_detail['about'];?></textarea>
              </div>
            </div>
             <div class="col-md-6">
              <div class="form-group">
                <label for="size">Address: </label>
                <textarea name="address" class="form-control" ><?= $d_modal_detail['address'];?></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Company Detail: </label>
                <textarea name="companydetail" class="form-control" ><?= $d_modal_detail['companydetail'];?></textarea>
              </div>
            </div>
            <div class="col-md-6 ">
            <div class="form-group">
              <img  >
             <img class="img img-responsive"  src="assets/img/donors/profile/<?= $d_modal_detail['profile']?>" id="uploadPreview"  height="100" width="100" class="img-thumb-modal"><br>
             <input type="file" name="img_donor" id="uploadImage" onchange="PreviewImage('uploadImage','uploadPreview')" class="form-control">
             </div>
            </div>
            
          </div>
        <div class="text-right">
        <button type="submit" class="btn btn-primary" id="edit_donations">Update</button>
        <button type="button" class="btn btn-secondary" onclick="closeModal()" data-dismiss="modal" data-backdrop="static" data-keyboard="false">Close</button>
                    </div>
        
    
    </form>
        </div>
        </div>
        </div>
        </div>
  <script type="text/javascript">
   
    function closeModal(){
      $('#detailModal').modal('hide')
      setTimeout(function(){
        $('#detailModal').remove();
      },500);
    }

    $(document).ready(function() {
       manage_Donor();

         $("#update_Donor").on('submit', function(e) {
          
    e.preventDefault();
               $.ajax({
                   url: 'Functions/Update.php?form=updatedonor',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
                      
                     
                   window.open('manageDonor.php','_self');
       $('.alert-success').html(result);
                      
                     
              // $('').html(result);
                    }
                })
   });
    });
  



</script>
  
