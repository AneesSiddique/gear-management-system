<?php 
include('../includes/connection.php');
    $id = $_POST['id'];
    // $id = (int)$id;

    $d_modal_sql = "SELECT donations.id, donations.amount,donations.paymentmode,donations.number,donations.donatedate,donors.name,donations.scannedimg,types.title from donations LEFT OUTER JOIN donors ON donations.donorid=donors.id LEFT OUTER JOIN types ON donations.type=types.id WHERE donations.id = $id";
    $d_modal_query = mysqli_query($con,$d_modal_sql);
    $d_modal_detail = mysqli_fetch_array($d_modal_query);

     $select=mysqli_query($con,"Select * from types");
     $fetch =mysqli_fetch_assoc($select)
//     $companyemail = $d_modal_detail['companyemail'];
//     $companyname = $d_modal_detail['companyname'];
//     $companylogo = $d_modal_detail['companylogo'];
//     $companydesc = $d_modal_detail['companydesc'];
//     $companycontact = $d_modal_detail['companycontact'];
//     $npnnumber = $d_modal_detail['npnnumber'];
//     $industry = $d_modal_detail['industry'];
//     $city = $d_modal_detail['city'];
//     $country = $d_modal_detail['country'];
//     $status = $d_modal_detail['status'];

 ?>
 <script type="text/javascript">
    function PreviewImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
 </script>
 <!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
         <h3 class="text-center"> Edit Donation</h3>
      <hr>
      </div>
  <form  method="POST" id="update_Donation" enctype="multipart/form-data" action="#">
     <div class="modal-body">
      <div class="container-fluid">
        <span id="alert_message" class="bg-danger"></span>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Donor Name: </label>
                <input type="hidden" name="txt_id" value="<?= $d_modal_detail['id'];?>">
                <input type="text" name="donor_name" class="form-control" value="<?= $d_modal_detail['name'];?>" disabled>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Donation As: </label>
                <select  name="type" class="form-control">
                <?php
                    $select=mysqli_query($con,"Select * from types");
                        while($fetch =mysqli_fetch_array($select)){
                       echo "<option value = '{$fetch['id']}'";
                      if ($d_modal_detail['title'] == $fetch['title'])
                          echo "selected = 'selected'";
                      echo ">{$fetch['title']}</option>";
                    } ?>
                  </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Amount: </label>
                <input type="text" name="donor_amount" class="form-control" value="<?= $d_modal_detail['amount'];?>" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Payment Mode: </label>
                <input type="text" name="payment_mode" class="form-control" value="<?= $d_modal_detail['paymentmode'];?>" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Chaque Number: </label>
                <input type="text" name="chaque_number" class="form-control" value="<?= $d_modal_detail['number'];?>" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="size">Donation Date: </label>
                <input type="date" name="donation_date" class="form-control" value="<?= $d_modal_detail['donatedate'];?>" >
              </div>
            </div>
           <!--  <div class="col-md-6">
              <div class="form-group">
                <select required="" id="donor" name="selectdonor" class="form-control">
                    <option label="Select Donor"></option>
                   <?php
                      $select_donors=mysqli_query($con,"Select * from donors");
                      while($fetch_donors =mysqli_fetch_array($select_donors)){
                       echo "<option value = '{$fetch_donors['name']}'";
                      if ($d_modal_detail['name'] == $fetch_donors['name'])
                          echo "selected = 'selected'";
                      echo ">{$fetch_donors['name']}</option>";
                    } ?>
                </select>
              </div>
            </div> -->
            <div class="col-md-6 ">
            <div class="form-group">
              <img  >
             <img class="img img-responsive"  src="assets/img/donations/scannedimg/<?= $d_modal_detail['scannedimg']?>" id="uploadPreview"  height="100" width="100" class="img-thumb-modal"><br>
             <input type="file" name="img_donations" id="uploadImage" onchange="PreviewImage('uploadImage','uploadPreview')" class="form-control">
             </div>
            </div>
            
          </div>
        <div class="text-right">
        <button type="submit" class="btn btn-primary" id="edit_donations">Update</button>
        <button type="button" class="btn btn-secondary" onclick="closeModal()" data-dismiss="modal" data-backdrop="static" data-keyboard="false">Close</button>
                    </div>
        
    
    </form>
        </div>
        </div>
        </div>
        </div>
  <script type="text/javascript">
   
    function closeModal(){
      $('#detailModal').modal('hide')
      setTimeout(function(){
        $('#detailModal').remove();
      },500);
    }

    $(document).ready(function() {
       manage_Donation();

         $("#update_Donation").on('submit', function(e) {
          
    e.preventDefault();
               $.ajax({
                   url: 'Functions/Update.php?form=updatedonation',
                    type: "POST",             
                    data: new FormData(this), 
                    contentType: false,       
                    cache: false,             
                    processData:false,        
                    success:function (result) {
                      
                     
                   window.open('manageDonation.php','_self');
       $('.alert-success').html(result);
                      
                     
              // $('').html(result);
                    }
                })
   });
    });
  



</script>
  
