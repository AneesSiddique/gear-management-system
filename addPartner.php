<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>
<script type="text/javascript">
	function PreviewImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
</script>
<body>
	<?php include('includes/nav.php') ?>

	<!-- /main navbar -->


	<!-- Page container -->
		<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper" >

				<!-- Content area -->
				<div class="content">
					<div class="col-md-12">

							<!-- Horizontal form -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Partner Donation</h5>
								</div>

								<div class="panel-body">
									<form class="form-horizontal" method="POST" id="partner_Donation" enctype="multipart/form-data">
									<div class="alert-success" ></div>
										<div class="row">
										<div class="col-md-6">
											<div class="form-group ">
											<label class="control-label ">Partner Assessment</label>
											<select class="form-control" name="partner_assess">
											 <option label="Select Partner"></option>
		           						<?php
								            $select=mysqli_query($con,"SELECT * FROM `partners`");
								                while($fetch =mysqli_fetch_array($select)){
								           		echo" <option value='".$fetch['id']."'>".$fetch['applicationholder']."</option>";
			                                } ?>
											</select>
											</div>
										</div>
										
										
										<div class="col-md-6">
											<div class="form-group ">
											<label class="control-label ">For Which Business</label>
											<input type="text" name="business_name" class="form-control" >
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group ">
											<label class="control-label ">Fund Donate</label>
											<input type="text" name="fund" class="form-control" >
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group ">
											<label class="control-label ">Volunteer Name</label>
											<select class="form-control" name="volunteer">
											 <option label="Select Volunteer"></option>
		           						<?php
								            $select=mysqli_query($con,"SELECT * FROM `volunteers`");
								                while($fetch =mysqli_fetch_array($select)){
								           		echo" <option value='".$fetch['id']."'>".$fetch['name']."</option>";
			                                } ?>
											</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group ">
											<label class="control-label ">Declaration of Zakat Upload Scanned Image</label>
											<img  id="form_uploadPreview"  height="200" width="200">
											<input id="form_uploadImage"  onchange="PreviewImage('form_uploadImage','form_uploadPreview')" type="file" class="form-control " name="Scanned_img">
											
											</div>
										</div>
									</div>
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
				
					



					<!-- Footer -->
					<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php } ?>