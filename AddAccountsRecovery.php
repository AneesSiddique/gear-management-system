<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>
<body>

	<!-- Main navbar -->
	<?php include('includes/nav.php') ?>

	<!-- /main navbar -->
<?php include('includes/sidebar_nav.php') ?>


	<!-- Page container -->
	


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-md-8">

							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add New Accounts Recovery</h5>
								</div>

								<div class="panel-body">
									<form class="form-horizontal" action="#">
										<div class="row"> 

									<div class="form-group col-md-6">
				                        	
				                        	<div class="col-lg-6">
				                        		<label >Select partner</label>
					                            <select name="select" class="form-control">
					                                <option value="opt1">Partner 1</option>
					                                <option value="opt2">Partner 2</option>
					                                <option value="opt3">Partner 3</option>
					                            </select>
				                            </div>
				                        </div>
				                        <div class="form-group col-md-6">
				                        	
				                        	<div class="col-lg-6">
				                        		<label >Select Case</label>
					                            <select name="select" class="form-control">
					                                <option value="opt1">case 1</option>
					                                <option value="opt2">case 2</option>
					                                <option value="opt3">case 3</option>
					                            </select>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="row ">
				                        <div class="form-group col-md-6">
										 <div class="col-lg-10">
												<label>search partner </label>
												<select class="select-multiple-tokenization" multiple="multiple">					
											
										</select>
											</div>
										</div>
										 <div class="form-group col-md-6">
										 <div class="col-lg-10">
												<label>search partner </label>
												<select class="select-multiple-tokenization" multiple="multiple">					
											
										</select>
											</div>
										</div>
									</div>
									<div class="row form-group">
									<div class="">
											<div class="col-lg-4">
					
												<label class="control-label col-lg-2">Date</label>
												<input type="date" class="form-control">
											</div>
										</div>
										<div class="">
											<div class="col-lg-4">
					
												
												<input type="radio" style="margin-top: 40px" >Today
											</div>
										</div>
									</div>

										<div class="form-group">
											
											<div class="col-lg-10">
												<label class="control-label col-lg-2">Recover Amount</label>
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group" id="hideshow" style="display: none">
				                          	<label class="control-label col-lg-2">&nbsp;</label>
				                          	 <img class="img img-responsive"   id="uploadPreview"  height="100" width="150" class="img-thumb-modal">
				                          	</div>
				                          <div class="form-group">
				                        	<label class="control-label col-lg-2">Upload scanned image</label>
				                        	<div class="col-lg-10">
				                        										
											
												<input type="file" accept="image/x-png, image/gif, image/jpeg" id="uploadImage" onchange="PreviewImage('uploadImage','uploadPreview')" class="btn btn-info form-control">
											</div>
										</div>

										

										<div class="text-right">
											<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
						<div class="col-lg-2"></div>
					</div>
				
					



					<!-- Footer -->
					<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php } ?>
