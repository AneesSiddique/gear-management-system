<?php 
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
// echo 'anees';
// echo $uri_segments[2];
// echo $uri_segments[0];
 ?>
<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-default">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><!-- <img src="assets/images/image.png" class="img-circle img-sm" alt=""> --> </a>
								<!-- <div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div> -->
<!-- 
								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div> -->
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li><a href="Dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li>
										<li>
											<a href="#"><i class="icon-cash3"></i><span>Donation</span></a>
											<ul>
												<li class="<?= $uri_segments[2]=='manageDonation.php'?'active':''?>"><a href="manageDonation.php" class="legitRipple">Manage Donation</a></li>
												<li class="<?= $uri_segments[2]=='addDonation.php'?'active':''?>"><a href="addDonation.php" class="legitRipple">Add New Donation</a></li>
											</ul>
										</li>
										<li>
											<a href="#"><i class="icon-cash"></i> <span>Donor</span></a>
											<ul>
												<li class="<?= $uri_segments[2]=='manageDonor.php'?'active':''?>"><a href="manageDonor.php" class="legitRipple">Manage Donor</a></li>
												<li class="<?= $uri_segments[2]=='addDonor.php'?'active':''?>"><a href="addDonor.php" class="legitRipple">Add New Donor</a></li>
											</ul>
										</li>
										<li>
											<a href="#"><i class="icon-cash"></i> <span>Volunteer</span></a>
											<ul>
												<li class="<?= $uri_segments[2]=='manageVolunteer.php'?'active':''?>"><a href="manageVolunteer.php" class="legitRipple">Manage Volunteer</a></li>
												<li class="<?= $uri_segments[2]=='addVolunteer.php'?'active':''?>"><a href="addVolunteer.php" class="legitRipple">Add New Volunteer</a></li>
											</ul>
										</li>
										<li>
											<a href="#"><i class="icon-users4"></i><span>Partner</span></a>
											<ul>
												<li class="<?= $uri_segments[2]=='managePartner.php'?'active':''?>"><a href="managePartner.php" class="legitRipple">Manage Partner</a></li>
												<li class="<?= $uri_segments[2]=='addPartner.php'?'active':''?>"><a href="addPartner.php" class="legitRipple">Add New Partner</a></li>
											</ul>
										</li>
										<li>
											<a href="#"><i class=" icon-user"></i><span>Partners Assessment </span></a>
											<ul>
												<li class="<?= $uri_segments[2]=='managePartnerAssessment.php'?'active':''?>"><a href="managePartnerAssessment.php" class="legitRipple">Manage List</a> </li>
												<li class="<?= $uri_segments[2]=='addPartnerAssessment.php'?'active':''?>"><a href="addPartnerAssessment.php" class="legitRipple">Add New </a></li>
											</ul>
										</li>
										
										<li>
											<a href="#"><i class=" icon-user"></i><span>Self Employment (Zakat)</span></a>
											<ul>
												<li class="<?= $uri_segments[2]=='manageZakat.php'?'active':''?>"><a href="manageZakat.php" class="legitRipple">Manage List</a> </li>
												<li class="<?= $uri_segments[2]=='addZakat.php'?'active':''?>"><a href="addZakat.php" class="legitRipple">Add New </a></li>
											</ul>
										</li>
										<li>
											<a href="#"><i class=" icon-user"></i><span>Self Employment (Sadaqah)</span></a>
											<ul>
												<li class="<?= $uri_segments[2]=='manageSadqah.php'?'active':''?>"><a href="manageSadqah.php" class="legitRipple">Manage List</a> </li>
												<li class="<?= $uri_segments[2]=='addSadaqah.php'?'active':''?>"><a href="addSadaqah.php" class="legitRipple">Add New </a></li>
											</ul>
										</li>
										<li>
											<a href="#"><i class=" icon-folder-search position-left"></i><span>Recovery </span></a>
											<ul>
												<li class="<?= $uri_segments[2]=='manageRecovery.php'?'active':''?>"><a href="manageRecovery.php">Manage List</a></li>
												<li class="<?= $uri_segments[2]=='AddAccountsRecovery.php'?'active':''?>"><a href="AddAccountsRecovery.php">Add New</a></li>
											</ul>
										</li>
										
										<li>
											<a href="#"><i class=" icon-coins"></i><span>Accounts </span></a>
											<ul>
												
												<li class="<?= $uri_segments[2]=='manageAccounts.php'?'active':''?>"><a href="manageAccounts.php" class="legitRipple">Manage List</a> </li>

												<li class="<?= $uri_segments[2]=='AddnewAccounts.php'?'active':''?>"><a href="AddnewAccounts.php" class="legitRipple">Add New </a></li>
											</ul>
										</li>										
										<li>
											<a href="#"><i class="icon-books position-left"></i><span>Reports</span></a>
											<ul>
												<li><a href="#">Reports Form Comming Soon</a></li>
											</ul>
										</li>
								</li>
								
								<!-- /main -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>