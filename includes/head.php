
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Gear Management System</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/script.js"></script>


	<!-- /theme JS files -->
  <link rel="stylesheet" href="dist/css/lightbox.min.css">

</head>

<script type="text/javascript">
	function PreviewImage(fileId,imgId) {

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById(fileId).files[0]);
            $('#hideshow').css('display', 'block');

            oFReader.onload = function (oFREvent) {
                document.getElementById(imgId).src = oFREvent.target.result;
            };
        }
</script>