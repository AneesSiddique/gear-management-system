<?php
include('includes/connection.php');
  	session_start();
if(!isset($_SESSION['user_email'])){
  
  header("location: index");
  
  }
  else{ 

 include('includes/head.php'); ?>
<body>
	<?php include('includes/nav.php') ?>

	<!-- /main navbar -->


	<!-- Page container -->
		<?php include('includes/sidebar_nav.php') ?>

			<!-- /main sidebar -->



			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">
					<div class="col-md-12">

							<!-- Horizontal form -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add New Head Of Accounts</h5>
								</div>

								<div class="panel-body">
									<form class="form-vertical" action="#">
									<div class="row">
										<div class="col-md-5">
											<div class="form-group ">
											<label class="control-label ">Name</label>
											<input type="text" class="form-control" >
											</div>
										</div>
										<div class="col-md-1"></div>
										<div class="col-md-5">
											<div class="form-group ">
											<label class="control-label ">Phone #</label>
											<input type="text" class="form-control" >
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-5">
											<div class="form-group ">
											<label class="control-label ">Email</label>
											<input type="text" class="form-control" >
											</div>
										</div>
										<div class="col-md-1"></div>
										<div class="col-md-5">
											<div class="form-group ">
											<label class="control-label ">Address #</label>
											<textarea  class="form-control" ></textarea>
											</div>
										</div>
									</div>
									
										
										<div class="row">
										<div class="col-md-5">
											<div class="form-group ">
											<label class="control-label "> Upload Profile Image</label>
											<input type="file" class="form-control" >
											</div>
										</div>
										
									</div>
										

										
										

										
										

										<div class="text-right">
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
							</div>
							<!-- /horizotal form -->
							</div>
				
					



					<!-- Footer -->
					<?php include('includes/footer.php') ?>

					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php } ?>